export const APPURL = "http://3.15.94.51:3001/api/v1/";
export const LOGIN = "login";
export const SIGNUP = "signup";
export const TOURNATMENT_LIST = "tournaments";
export const TOURNAMENT_TEAM_LIST = "tournament/teams";
export const FINAL_SUBMIT   =   "survey";
export const HISTORY_COUNT = "total_surveys/"

export const LOGOUT = "logout";

export const TEAMANAME    =   "firstTeamName";
export const TEAMBNAME    =   "secondTeamName";
export const TOURNAMENT_DETAILS    =   "Tournament";
export const TOURNAMENT_DATE    =   "Tournament_date";
export const INCIDENT_REPORT =   "Incident_Report";
export const SELECTED_TEAMS = "Selected Teams";
export const GAME_TYPE = "TournamentType";
export const GENDER_TYPE = "TournamentGender";
export const AGE_GROUP = "TournamentAgeGroup"
export const TEAMA_SURVEY_DONE  =   "TeamASurvey";
export const TEAMB_SURVEY_DONE  =   "TeamBSurvey";
export const COMPETITION_LEVEL = "TournamentCompetitionLevel";

export const TEAMA_COACH_IMPROVEMENT_REQUIRED =   "Team A coach improvement required"
export const TEAMA_ASTTCOACH_IMPROVEMENT_REQUIRED =   "Team A assttcoach improvement required"
export const TEAMA_FANS_IMPROVEMENT_REQUIRED  =   "Team A fans improvement required"
export const TEAMA_PLAYERS_IMPROVEMENT_REQUIRED   =   "Team A players improvement required"


export const IMPROVEMENT_SOLUTION = "Improvement solution"
export const INTERNET_CONNECTION_ISSUE = "Connect to the network to Sign in.";
export const APP_DISPLAY_NAME   =   "GOPRO";

export const HEAD_COACH_BEHAVIOUR   =   " head coach behaviour";
export const ASTT_COACH_BEHAVIOUR   =   " assistant coach behaviour";
export const FANS_BEHAVIOUR   =   " fans behaviour";
export const PLAYER_BEHAVIOUR   =   " players behaviour";

export const TEAMA_OFFICIALS_FEEDBACK  =   "TeamAOfficialsFeedback";
export const TEAMA_OPPONENT_FEEDBACK  =   "TeamAOpponentFeedback";
export const TEAMA_PLAYERS_FEEDBACK  =   "TeamAPlayersFeedback";
export const TEAMA_RULES_FEEDBACK  =   "TeamARulesFeedback";
export const TEAMB_OFFICIALS_FEEDBACK  =   "TeamBOfficialsFeedback";
export const TEAMB_OPPONENT_FEEDBACK  =   "TeamBOpponentFeedback";
export const TEAMB_PLAYERS_FEEDBACK  =   "TeamBPlayersFeedback";
export const TEAMB_RULES_FEEDBACK  =   "TeamBRulesFeedback";

export const NAME_VALIDATION_MESSAGE = "Name field can't be blank. Please enter valid name";

export const LOGIN_ERROR = "Invalid credentials. Please try again.";
export const SIGNUP_ERROR = "Signup not successful. Please try again later.";
export const EMAIL_EMPTY_MESSAGE = "Username must be an email and can't be empty";
export const EMAIL_VALIDATION_MESSAGE = "Email is not valid. Please enter valid email address.";
export const BLANK_EMAIL_ID_MESSAGE = "Email Id can't be blank.";
export const PASSWORD_EMPTY_MESSAGE = "Password can't be empty.";
export const CONFIRM_PASSWORD_EMPTY_MESSAGE = "Confirm password field can't be empty.";
export const PASSWORD_VALIDATION_MESSAGE = "Password length cannot be less than 8 characters.";
export const CNF_PASSWORD_VALIDATION_MESSAGE = "Password and Confirm Password didn't match. Please enter again.";
export const NAME_VALIDATION_DIGIT = "Name field only characters are allowed. Please enter valid name.";
export const PHONE_NUMBER_EMPTY = "Phone number can't be empty.";
export const PHONE_NUMBER_DIGIT_ONLY = "Phone number must contain only numbers.";


export const TAKE_REPORT_DATA = "Take report data";
