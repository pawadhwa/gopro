import {AppRegistry} from 'react-native';
import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Welcome from '../Login/Welcome';
import SignIn from '../Login/SignIn';
import SignUp from '../Login/SignUp';
import Home from '../Workflow/Home';
import TournamentList from '../Workflow/TournamentList';
import TeamInfo from '../Workflow/TeamInfo';
import TournamentSchedule from "../Workflow/TournamentSchedule";
import TeamExperience from "../Workflow/TeamExperience";
import FinalSubmission from "../Workflow/FinalSubmission";
import MoreTeamABehaviour from "../Workflow/MoreTeamABehaviour";
import MoreTeamBBehaviour from "../Workflow/MoreTeamBBehaviour";
import ImprovementScreenA from "../Workflow/ImprovementScreenA";
import FeedbackScreen from "../Workflow/FeedbackScreen";
import ImprovementSolution from "../Workflow/ImprovementSolution";
import ImprovementSolGame from "../Workflow/ImprovementSolGame";
import ImprovementSolOfficials from "../Workflow/ImprovementSolOfficials";
import ImprovementSolRules from "../Workflow/ImprovementSolRules";
import ImprovementSolPlayers from "../Workflow/ImprovementSolPlayers";
import ImprovementSolOpponent from "../Workflow/ImprovementSolOpponent";
import TournamentType from "../Workflow/TournamentType";
import SelectGender from "../Workflow/SelectGender";
import SelectAge from "../Workflow/SelectAge";
import CompetitionLevel from "../Workflow/CompetitionLevel";
import ManualTeamEntry from "../Workflow/ManualTeamEntry";

export const AppStackNavigator = {
  Welcome: {
    screen: Welcome,
    navigationOptions: {
      header: null,
    },
  },
  SignIn: {
    screen: SignIn,
    navigationOptions: {
      header: null,
    },
  },
  SignUp: {
    screen: SignUp,
    navigationOptions: {
      header: null,
    },
  },
  Home: {
    screen: Home,
    navigationOptions: {
      header: null,
    },
  },
  TournamentList: {
    screen: TournamentList,
    navigationOptions: {
      header: null,
    },
  },
  TeamInfo: {
    screen: TeamInfo,
    navigationOptions: {
      header: null,
    },
  },
  TournamentSchedule: {
    screen: TournamentSchedule,
    navigationOptions: {
      header: null
    },
  },
  TeamExperience: {
    screen: TeamExperience,
    navigationOptions: {
      header: null
    },
  },
  FinalSubmission : {
    screen: FinalSubmission,
    navigationOptions: {
      header: null
    },
  },
  MoreTeamABehaviour : {
    screen: MoreTeamABehaviour,
    navigationOptions: {
      header: null
    },
  },
  MoreTeamBBehaviour : {
    screen: MoreTeamABehaviour,
    navigationOptions: {
      header: null
    },
  },
  ImprovementScreenA: {
    screen: ImprovementScreenA,
    navigationOptions: {
      header: null
    },
  },
  ImprovementScreenB: {
    screen: ImprovementScreenA,
    navigationOptions: {
      header: null
    },
  },
  ImprovementSolGame: {
    screen: ImprovementSolGame,
    navigationOptions: {
      header: null
    },
  },
  ImprovementSolGameA: {
    screen: ImprovementSolGame,
    navigationOptions: {
      header: null
    },
  },
  ImprovementSolOfficials: {
    screen: ImprovementSolOfficials,
    navigationOptions: {
      header: null
    },
  },
  ImprovementSolOfficialsA: {
    screen: ImprovementSolOfficials,
    navigationOptions: {
      header: null
    },
  },
  ImprovementSolOpponent: {
    screen: ImprovementSolOpponent,
    navigationOptions: {
      header: null
    },
  },
  ImprovementSolOpponentA: {
    screen: ImprovementSolOpponent,
    navigationOptions: {
      header: null
    },
  },
  ImprovementSolPlayers: {
    screen: ImprovementSolPlayers,
    navigationOptions: {
      header: null
    },
  },
  ImprovementSolPlayersA: {
    screen: ImprovementSolPlayers,
    navigationOptions: {
      header: null
    },
  },
  ImprovementSolRules: {
    screen: ImprovementSolRules,
    navigationOptions: {
      header: null
    },
  },
  ImprovementSolRulesA: {
    screen: ImprovementSolRules,
    navigationOptions: {
      header: null
    },
  },
  ImprovementSolution: {
    screen: ImprovementSolution,
    navigationOptions: {
      header: null
    }
  },
  FeedbackScreen: {
    screen: FeedbackScreen,
    navigationOptions: {
      header:null
    }
  },
  TournamentType: {
    screen: TournamentType,
    navigationOptions: {
      header: null
    }
  },
  SelectGender: {
    screen: SelectGender,
    navigationOptions: {
      header: null
    }
  },
  SelectAge: {
    screen: SelectAge,
    navigationOptions: {
      header: null
    }
  },
  CompetitionLevel: {
    screen: CompetitionLevel,
    navigationOptions: {
      header:null
    }
  },
    ManualTeamEntry: {
        screen: ManualTeamEntry,
        navigationOptions: {
            header:null
        }
    }
};

export const appContainer = (isUserLogIn = false) => {
  return createAppContainer(
    createStackNavigator(AppStackNavigator, {
      headerMode: 'screen',
      initialRouteName: isUserLogIn ? 'Home' : 'Welcome',
    }),
  );
};
