import {Component} from "react";
import RNFetchBlob from "react-native-fetch-blob";
import {
    APPURL,
    LOGIN,
    SIGNUP,
    TOURNATMENT_LIST,
    TOURNAMENT_TEAM_LIST,
    CHANGEPASSWORD,
    WALLET_STATUS,
    STEPS_COUNT,
    HEARTRATE_COUNT,
    RECENT_ACTIVITIES,
    LOGOUT,
    SLEEP_HOURS,
    CALORIES_COUNT,
    FORGOT_PASSWORD,
    YESTERDAY_HOURS,
    GET_PARTNERS,
    LINK_PARTNER,
    DELINK_PARTNER,
    GLOBAL_CONFIG,
    PROGRAM_LIST, LINK_PARTNER_AUTH, PROGRAMS, INTERNET_CONNECTION_ISSUE, FINAL_SUBMIT, HISTORY_COUNT,
} from '../../Constants/StringConstants';
import NetInfo,{NetInfoStateType} from '@react-native-community/netinfo';
import {Alert, Platform} from 'react-native';

/*
export async function performLogin(username, password) {
    let ifConnected = false;
    let res = null;
    await NetInfo.fetch().then(state => {
        ifConnected = state.isConnected;
        if (ifConnected)
        {
            let dataToServer = {email: username, password: password};
            let loginURL = APPURL + LOGIN;
            return RNFetchBlob.config({
                trusty: true
            })
                .fetch('POST', loginURL, {
                    'Content-Type': 'application/json',
                }, JSON.stringify(dataToServer))
                .then(response => response.json())
                .then((response) => {
                    console.log("==================>>>>>>>")
                    console.log(JSON.stringify(response));
                })
                .catch(error => {
                    res =  {success: false}
                });
        }
    });
}
*/


export async function performSignup(data) {
    console.log("Sgning Up, sending data => \n"+JSON.stringify(data));

    let ifConnected = false;
    let res = null;
    await NetInfo.fetch().then(state => {
        ifConnected = state.isConnected;
        if (ifConnected)
        {
            let dataToServer = data;

            let signUpURL = APPURL + SIGNUP;
            return RNFetchBlob.config({
                trusty: true
            })
                .fetch('POST', signUpURL, {
                    'Content-Type': 'application/json',
                }, JSON.stringify(dataToServer))
                .then(response => response.json())
                .then((response) => {
                    console.log("Networking file => "+JSON.stringify(response));
                    if ( response.statusCode && response.statusCode === 200) {
                        res = {success: true, message: response.message}
                    } else {
                        res = {success: false, message:response.message};
                    }
                })
                .catch(error=>{
                    res = {success: false}
                });
        } else {
            res = {success: false, message:INTERNET_CONNECTION_ISSUE}
        }
    });
    return res;
}


export async function performLogin(username, password) {

    let ifConnected = false;
    let res = null;
    await NetInfo.fetch().then(state => {
        ifConnected = state.isConnected;
        if (ifConnected)
        {
            let dataToServer = {email: username, password: password};
            let loginURL = APPURL + LOGIN;

            return RNFetchBlob.config({
                trusty: true
            })
                .fetch('POST', loginURL, {
                    'Content-Type': 'application/json',
                }, JSON.stringify(dataToServer))
                .then(response => response.json())
                .then((response) => {
                    console.log("Networking file => "+JSON.stringify(response));
                    if (response.statusCode && response.statusCode === 200) {
                        res =  {success: true, data: response.result}
                    } else {
                        res =  {success: false,message: response.message}
                    }
                })
                .catch(error => {
                    res =  {success: false}
                });
        } else {
            res = {success: false, message:INTERNET_CONNECTION_ISSUE}
        }
    });
    return res;
}

export async function getHistory(authToken,userID) {
    console.log("Networking file, getTournamentList() function called \n authToken = "+authToken);

    let ifConnected = false;
    let res = null;
    await NetInfo.fetch().then(state => {
        ifConnected = state.isConnected;
        if (ifConnected)
        {
            //let dataToServer = {email: username, password: password};
            let url = APPURL + HISTORY_COUNT + userID;

            return RNFetchBlob.config({
                trusty: true
            })
                .fetch('POST', url, {
                    'X_Auth_Token': authToken,
                    'Content-Type': 'application/json',
                })
                .then(response => response.json())
                .then((response) => {
                    console.log("Networking file, get History => "+JSON.stringify(response));
                    if (response.statusCode && response.statusCode === 200) {
                        res =  {success: true, data: response.result}
                    } else {
                        res =  {success: false,message: response.message}
                    }
                })
                .catch(error => {
                    // console.log("Networking file, getTournamentList => error => "+JSON.stringify(error));
                    res =  {success: false}
                });
        } else {
            res = {success: false, message:INTERNET_CONNECTION_ISSUE}
        }
    });
    return res;
}


export async function getTournamentList(authToken) {
    console.log("Networking file, getTournamentList() function called \n authToken = "+authToken);

    let ifConnected = false;
    let res = null;
    await NetInfo.fetch().then(state => {
        ifConnected = state.isConnected;
        if (ifConnected)
        {
            //let dataToServer = {email: username, password: password};
            let url = APPURL + TOURNATMENT_LIST;

            return RNFetchBlob.config({
                trusty: true
            })
                .fetch('GET', url, {
                    'X_Auth_Token': authToken,
                    'Content-Type': 'application/json',
                }, '')
                .then(response => response.json())
                .then((response) => {
                    console.log("Networking file, getTournamentList => "+JSON.stringify(response));
                    if (response.statusCode && response.statusCode === 200) {
                        res =  {success: true, data: response.result}
                    } else {
                        res =  {success: false,message: response.message}
                    }
                })
                .catch(error => {
                    // console.log("Networking file, getTournamentList => error => "+JSON.stringify(error));
                    res =  {success: false}
                });
        } else {
            res = {success: false, message:INTERNET_CONNECTION_ISSUE}
        }
    });
    return res;
}




export async function getTournamentTeamList(authToken, tournamentId, date) {
    console.log("Networking file, getTournamentTeamList() function called \n authToken = "+authToken+"\n id = "+tournamentId+" ,\n date = "+date);

    let ifConnected = false;
    let res = null;
    await NetInfo.fetch().then(state => {
        ifConnected = state.isConnected;
        if (ifConnected)
        {
            let dataToServer = {tournament_id: tournamentId, date: date};
            let url = APPURL + TOURNAMENT_TEAM_LIST;
            return RNFetchBlob.config({
                trusty: true
            })
                .fetch('POST', url, {
                    'X_Auth_Token': authToken,
                    'Content-Type': 'application/json',
                }, JSON.stringify(dataToServer))
                .then(response => response.json())
                .then((response) => {

                    console.log("Networking file, getTournamentTeamList => "+JSON.stringify(response));
                    if (response.statusCode && response.statusCode === 200) {
                        res =  {success: true, data: response.result}
                    } else {
                        res =  {success: false,message: response.message}
                    }
                })
                .catch(error => {
                    //console.log("Networking file, getTournamentTeamList => error => "+JSON.stringify(error));
                    res =  {success: false}
                });
        } else {
            res = {success: false, message:INTERNET_CONNECTION_ISSUE}
        }
    });
    return res;
}


export async function finalSubmitData(authToken, tournamentId, date, teamAId, teamBId, userID, finalData) {
    console.log("Networking file, getTournamentTeamList() function called \n authToken = "+authToken+"\n id = "+tournamentId+" ,\n date = "+date);

    let ifConnected = false;
    let res = null;
    await NetInfo.fetch().then(state => {
        ifConnected = state.isConnected;
        if (ifConnected)
        {
            let dataToServer = {tournament_id: tournamentId, match_date: date, team1_id: teamAId, team2_id: teamBId, user_id: userID, survey_details: JSON.stringify(finalData)};
            console.log('########################### Final Submission of Data to server is==>' + JSON.stringify(dataToServer));
            let url = APPURL + FINAL_SUBMIT;
            return RNFetchBlob.config({
                trusty: true
            })
                .fetch('POST', url, {
                    'X_Auth_Token': authToken,
                    'Content-Type': 'application/json',
                }, JSON.stringify(dataToServer))
                .then(response => response.json())
                .then((response) => {

                    console.log("Networking file, getTournamentTeamList => "+JSON.stringify(response));
                    if (response.statusCode && response.statusCode === 200) {
                        res =  {success: true, data: response.result}
                    } else {
                        res =  {success: false,message: response.message}
                    }
                })
                .catch(error => {
                    //console.log("Networking file, getTournamentTeamList => error => "+JSON.stringify(error));
                    res =  {success: false}
                });
        } else {
            res = {success: false, message:INTERNET_CONNECTION_ISSUE}
        }
    });
    return res;
}
