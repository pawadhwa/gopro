import React, {Component} from 'react';
import {
    AppRegistry,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    Image,
    FlatList,
    ScrollView, AsyncStorage, Alert, ActivityIndicator
} from 'react-native';
import SurveyTopBar from '../../Components/SurveyTopBar';
import {NavigationActions, StackActions} from "react-navigation";
import {
    APP_DISPLAY_NAME, GAME_TYPE,
    IMPROVEMENT_SOLUTION,
    INCIDENT_REPORT, INTERNET_CONNECTION_ISSUE,
    SELECTED_TEAMS, TAKE_REPORT_DATA,
    TOURNAMENT_DATE,
    TOURNAMENT_DETAILS
} from "../../Constants/StringConstants";
import {fetchFromLocal, resetLocalStorage, saveToLocal} from "../../Utils/SaveDetails";
import {finalSubmitData, getTournamentList} from "../Navigation/Networking";
import {showInfoAlert} from "../../Utils/AlertPopUp";

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;

export default class FinalSubmission extends Component {
    constructor(props) {
        super(props);
        this.state = {
            teamData: [
                {name: 'Date:'},
                {name: 'Tournament:'},
                {name: 'Teams:'},
                {name: 'TeamAs Behaviour:'},
                {name: 'TeamDs Behaviour:'},
                {name: 'TeamA head coach:'},
                {name: 'TeamA assistant coach:'},
                {name: 'TeamA fans:'},
                {name: 'TeamA players:'},
                {name: 'TeamA head coach:'},
                {name: 'TeamA assistant coach:'},
                {name: 'TeamA fans:'},
                {name: 'TeamA players:'},
                {name: 'Incident Report:'},
            ],
            tournamentDate:'',
            isLoading:false
        };
        this.tournamentInfo = null;
        this.date = null;
        this.teamAName = null;
        this.teamBName = null;
        this.behaviourAAnswer = null;
        this.behaviourBAnswer = null;

        this.AheadCoachInfo = null;
        this.AassistantCoachInfo = null;
        this.AfansBehaviour = null;
        this.AplayersBehaviour = null;

        this.BheadCoachInfo = null;
        this.BassistantCoachInfo = null;
        this.BfansBehaviour = null;
        this.BplayersBehaviour = null;

        this.improvement = null;
        this.improvementSolution = null;
        this.incidentReport = null;

    }

    async componentDidMount() {

        let gameTypeValue = await fetchFromLocal(GAME_TYPE);
        console.log('JSON value is =====>' + gameTypeValue);
        let gameValue = JSON.parse(gameTypeValue);
        let tempData = gameValue[0];
        if (tempData.name === 'Tournament') {
            this.tournamentInfo = await fetchFromLocal(TOURNAMENT_DETAILS);
        } else {
            let tournamentDetails = {};
            tournamentDetails["tournament_name"] = tempData.name;
            tournamentDetails["id"] = 1;
            this.tournamentInfo = JSON.stringify(tournamentDetails);
        }


        this.date = await fetchFromLocal(TOURNAMENT_DATE);

        let selectedTeamData = await fetchFromLocal(SELECTED_TEAMS)
        let teamsData = await JSON.parse(selectedTeamData);
        this.teamAName = teamsData[0].team_name;
        this.teamBName = teamsData[1].team_name;
        let behaviourAQuestion = this.teamAName + " behaviour"
        let behaviourBQuestion = this.teamBName + " behaviour"

        this.behaviourAAnswer = await fetchFromLocal(behaviourAQuestion)
        this.behaviourBAnswer = await fetchFromLocal(behaviourBQuestion)

        let question1 = this.teamAName + " head coach behaviour";
        let question2 = this.teamAName + " assistant coach behaviour";
        let question3 = this.teamAName + " fans behaviour";
        let question4 = this.teamAName + " players behaviour";


        let questionB1 = this.teamBName + " head coach behaviour";
        let questionB2 = this.teamBName + " assistant coach behaviour";
        let questionB3 = this.teamBName + " fans behaviour";
        let questionB4 = this.teamBName + " players behaviour";

         this.AheadCoachInfo = await fetchFromLocal(question1);
         console.log('=============>');
         this.AassistantCoachInfo = await fetchFromLocal(question2);
        console.log('=============>');
         this.AfansBehaviour = await fetchFromLocal(question3);
        console.log('=============>');
         this.AplayersBehaviour = await fetchFromLocal(question4);
        console.log('=============>');

        this.BheadCoachInfo = await fetchFromLocal(questionB1);
        console.log('=============>');
        this.BassistantCoachInfo = await fetchFromLocal(questionB2);
        console.log('=============>');
        this.BfansBehaviour = await fetchFromLocal(questionB3);
        console.log('=============>');
        this.BplayersBehaviour = await fetchFromLocal(questionB4);
        console.log('=============>');

         //this.improvement = await fetchFromLocal(IMPROVEMENT_SECTION);
        console.log('=============>');
         //this.improvementSolution = await fetchFromLocal(IMPROVEMENT_SOLUTION);
        console.log('=============>');
         this.incidentReport = await fetchFromLocal(INCIDENT_REPORT);
        console.log('=============>');
         this.setAnswersToQuestions()

    }

    getImageName(type){
        if (type === 'great'){
            return require('../../Images/Icons/Great.png');
        }
        else if (type === 'okay'){
            return require('../../Images/Icons/Okay.png');
        }
        else if (type === 'bad'){
            return require('../../Images/Icons/Bad.png');
        }
        else if (type === 'terrible'){
            return require('../../Images/Icons/Terrible.png');
        }
        else {
            return require('../../Images/Icons/Good.png');
        }
    }

    async startSurvey(){

        let tAgameMessageString = 'TEAMA_RESPECT_GAME_REQUIRED';
        let tAplayersMessageString = 'TEAMA_RESPECT_PLAYERS_REQUIRED';
        let tArulesMessageString = 'TEAMA_RESPECT_RULES_REQUIRED';
        let tAopponentMessageString = 'TEAMA_RESPECT_OPPONENT_REQUIRED';
        let tAofficialMessageString = 'TEAMA_RESPECT_OFFICIAL_REQUIRED';

        let tBgameMessageString = 'TEAMB_RESPECT_GAME_REQUIRED';
        let tBplayersMessageString = 'TEAMB_RESPECT_PLAYERS_REQUIRED';
        let tBrulesMessageString = 'TEAMB_RESPECT_RULES_REQUIRED';
        let tBopponentMessageString = 'TEAMB_RESPECT_OPPONENT_REQUIRED';
        let tBofficialMessageString = 'TEAMB_RESPECT_OFFICIAL_REQUIRED';


        let teamAGameRecord = await fetchFromLocal(tAgameMessageString)
        let teamAGameFeedbackRequired = JSON.parse(teamAGameRecord);
        let improvementData = [];

        if (teamAGameFeedbackRequired) {
            let saveGameKey = this.teamAName + '_GAME_SUGGESTION'
            let answerValue = await fetchFromLocal(saveGameKey);
            let submitstring = this.teamAName + ' game suggestions'
            let testObj = {submitstring:answerValue};
            improvementData = [...improvementData, testObj];
        }

        console.log('######################')

        let teamAPlayerRecord = await fetchFromLocal(tAplayersMessageString)
        let teamAPlayerFeedbackRequired = JSON.parse(teamAPlayerRecord);
        if (teamAPlayerFeedbackRequired) {
            let savePlayersKey = this.teamAName + '_PLAYERS_SUGGESTION'
            let answerValue = await fetchFromLocal(savePlayersKey);
            let submitstring = this.teamAName + ' players suggestions'
            let testObj = {};
            testObj[submitstring] = answerValue;
            improvementData = [...improvementData, testObj];
        }

        console.log('######################')

        let teamARulesRecord = await fetchFromLocal(tArulesMessageString)
        let teamARulesFeedbackRequired = JSON.parse(teamARulesRecord);
        if (teamARulesFeedbackRequired) {
            let saveRulesKey = this.teamAName + '_RULES_SUGGESTION'
            let answerValue = await fetchFromLocal(saveRulesKey);
            let submitstring = this.teamAName + ' rules suggestions'
            let testObj = {};
            testObj[submitstring] = answerValue;
            improvementData = [...improvementData, testObj];
        }
        console.log('######################')

        let teamAOpponentRecord = await fetchFromLocal(tAopponentMessageString)
        let teamAOpponentFeedbackRequired = JSON.parse(teamAOpponentRecord);
        if (teamAOpponentFeedbackRequired) {
            let saveOpponentKey = this.teamAName + '_OPPONENT_SUGGESTION'
            let answerValue = await fetchFromLocal(saveOpponentKey);
            let submitstring = this.teamAName + ' opponent suggestions'
            let testObj = {};
            testObj[submitstring] = answerValue;
            improvementData = [...improvementData, testObj];
        }
        console.log('######################')

        let teamAOfficialRecord = await fetchFromLocal(tAofficialMessageString)
        let teamAOfficialFeedbackRequired = JSON.parse(teamAOfficialRecord);
        if (teamAOfficialFeedbackRequired) {
            let saveOfficialKey = this.teamAName + '_OFFICIALS_SUGGESTION'
            let answerValue = await fetchFromLocal(saveOfficialKey);
            let submitstring = this.teamAName + ' official suggestions'
            let testObj = {};
            testObj[submitstring] = answerValue;
            improvementData = [...improvementData, testObj];
        }
        console.log('######################')

        let teamBGameRecord = await fetchFromLocal(tBgameMessageString)
        let teamBGameFeedbackRequired = JSON.parse(teamBGameRecord);
        if (teamBGameFeedbackRequired){
            let saveGameKey = this.teamBName + '_GAME_SUGGESTION'
            let answerValue = await fetchFromLocal(saveGameKey);
            let submitstring = this.teamBName + ' game suggestions'
            let testObj = {};
            testObj[submitstring] = answerValue;
            improvementData = [...improvementData, testObj];
        }
        console.log('######################')

        let teamBPlayersRecord = await fetchFromLocal(tBplayersMessageString)
        let teamBPlayerFeedbackRequired = JSON.parse(teamBPlayersRecord);
        if (teamBPlayerFeedbackRequired) {
            let savePlayersKey = this.teamBName + '_PLAYERS_SUGGESTION'
            let answerValue = await fetchFromLocal(savePlayersKey);
            let submitstring = this.teamBName + ' players suggestions'
            let testObj = {};
            testObj[submitstring] = answerValue;
            improvementData = [...improvementData, testObj];
        }
        console.log('######################')

        let teamBRulesRecord = await fetchFromLocal(tBrulesMessageString)
        let teamBRulesFeedbackRequired = JSON.parse(teamBRulesRecord);
        if (teamBRulesFeedbackRequired) {
            let saveRulesKey = this.teamAName + '_RULES_SUGGESTION'
            let answerValue = await fetchFromLocal(saveRulesKey);
            let submitstring = this.teamBName + ' rules suggestions'
            let testObj = {};
            testObj[submitstring] = answerValue;
            improvementData = [...improvementData, testObj];
        }
        console.log('######################')

        let teamBOpponentRecord = await fetchFromLocal(tBopponentMessageString)
        let teamBOpponentFeedbackRequired = JSON.parse(teamBOpponentRecord);
        if (teamBOpponentFeedbackRequired) {
            let saveOpponentKey = this.teamBName + '_OPPONENT_SUGGESTION'
            let answerValue = await fetchFromLocal(saveOpponentKey);
            let submitstring = this.teamBName + ' opponent suggestions'
            let testObj = {};
            testObj[submitstring] = answerValue;
            improvementData = [...improvementData, testObj];
        }
        console.log('######################')

        let teamBOfficialRecord = await fetchFromLocal(tBofficialMessageString)
        let teamBOfficialFeedbackRequired = JSON.parse(teamBOfficialRecord);
        if (teamBOfficialFeedbackRequired) {
            let saveOfficialKey = this.teamAName + '_OFFICIALS_SUGGESTION'
            let answerValue = await fetchFromLocal(saveOfficialKey);
            let submitstring = this.teamBName + ' official suggestions'
            let testObj = {};
            testObj[submitstring] = answerValue;
            improvementData = [...improvementData, testObj];
        }
        console.log('######################');

        console.log('Improvement data is ===============>' + JSON.stringify(improvementData));
        return improvementData;
    }

    async setAnswersToQuestions() {
        var teamCollection = [];
        for (let i = 0; i < this.state.teamData.length; i++) {
            var teamInfo = this.state.teamData[i];
            if (i === 0) {
                teamInfo.value = this.date;
                teamInfo.image = require('../../Images/Icons/Edit.png')
            } else if (i === 1) {
                let tournamentDetail = JSON.parse(this.tournamentInfo);
                teamInfo.value = tournamentDetail.tournament_name;
                teamInfo.image = require('../../Images/Icons/Edit.png')
            } else if (i === 2) {
                teamInfo.value = this.teamAName + ', ' + this.teamBName;
                teamInfo.image = require('../../Images/Icons/Edit.png')
            } else if (i === 3) {
                teamInfo.name = this.teamAName + ' Behaviour:';
                teamInfo.value = this.behaviourAAnswer;
                teamInfo.image = this.getImageName(this.behaviourAAnswer)
            } else if (i === 4) {
                teamInfo.name = this.teamBName + ' Behaviour:';
                teamInfo.value = this.behaviourBAnswer;
                teamInfo.image = this.getImageName(this.behaviourBAnswer)
            } else if (i === 5) {
                teamInfo.name = this.teamAName + ' Head coach:';
                teamInfo.value = this.AheadCoachInfo;
                teamInfo.image = this.getImageName(this.AheadCoachInfo);
            } else if (i === 6) {
                teamInfo.name = this.teamAName + ' assistant coach:';
                teamInfo.value = this.AassistantCoachInfo;
                teamInfo.image = this.getImageName(this.AassistantCoachInfo);
            } else if (i === 7) {
                teamInfo.name = this.teamAName + ' fans:';
                teamInfo.value = this.AfansBehaviour;
                teamInfo.image = this.getImageName(this.AfansBehaviour);
            } else if (i === 8) {
                teamInfo.name = this.teamAName + ' players:';
                teamInfo.value = this.AplayersBehaviour;
                teamInfo.image = this.getImageName(this.AplayersBehaviour);
            } else if (i === 9) {
                teamInfo.name = this.teamBName + ' Head coach:';
                teamInfo.value = this.BheadCoachInfo;
                teamInfo.image = this.getImageName(this.BheadCoachInfo);
            } else if (i === 10) {
                teamInfo.name = this.teamBName + ' assistant coach:';
                teamInfo.value = this.BassistantCoachInfo;
                teamInfo.image = this.getImageName(this.BassistantCoachInfo);
            } else if (i === 11) {
                teamInfo.name = this.teamBName + ' fans:';
                teamInfo.value = this.BfansBehaviour;
                teamInfo.image = this.getImageName(this.BfansBehaviour);
            } else if (i === 12) {
                teamInfo.name = this.teamBName + ' players:';
                teamInfo.value = this.BplayersBehaviour;
                teamInfo.image = this.getImageName(this.BplayersBehaviour);
            } else if (i === 13) {
                teamInfo.value = this.incidentReport;
                teamInfo.image = require('../../Images/Icons/Edit.png')
            }
            teamCollection.push(teamInfo);
        }
        this.setState({teamsData: []});


        let selectedTeamData = await fetchFromLocal(SELECTED_TEAMS)
        let teamsData = await JSON.parse(selectedTeamData);
        let teamABehaviour = teamsData[0].team_name + " behaviour";
        let teamBBehaviour = teamsData[1].team_name + " behaviour";

        let teamAAnswer = await fetchFromLocal(teamABehaviour)
        let teamBAnswer = await fetchFromLocal(teamBBehaviour)

        let reportCount = await fetchFromLocal(TAKE_REPORT_DATA);
        let feedbackRequired = reportCount ? JSON.parse(reportCount) : 0;

        if (((teamAAnswer === 'good') || (teamAAnswer === 'great') || (teamAAnswer === 'okay')) && ((teamBAnswer === 'good') || (teamBAnswer === 'great') || (teamBAnswer === 'okay'))) {
            teamCollection.length = 5;
            this.setState({teamData: teamCollection});
        } else if (((teamAAnswer === 'terrible') || (teamAAnswer === 'bad')) && ((teamBAnswer === 'terrible') || (teamBAnswer === 'bad'))) {
            await this.setState({teamData: teamCollection});
            await this.updateFeedbackData(feedbackRequired, teamCollection);
        } else if (((teamAAnswer === 'good') || (teamAAnswer === 'great') || (teamAAnswer === 'okay')) && ((teamBAnswer === 'terrible') || (teamBAnswer === 'bad'))) {
            let remainingArr = teamCollection.splice(5, 4);
            await this.setState({teamData: teamCollection});
            await this.updateFeedbackData(feedbackRequired, teamCollection);
        } else if (((teamAAnswer === 'terrible') || (teamAAnswer === 'bad')) && ((teamBAnswer === 'good') || (teamBAnswer === 'great') || (teamBAnswer === 'okay'))) {
            let remainingArr = teamCollection.splice(9, 4);
            await this.setState({teamData: teamCollection});
            await this.updateFeedbackData(feedbackRequired, teamCollection);
        }
    }

    updateFeedbackData(feedbackRequired, teamCollection) {
        if (feedbackRequired > 0) {
            //Cancel
        } else {
            let remainingArr = teamCollection.pop();
            console.log('Collection data is =======>' + JSON.stringify(teamCollection));
            this.setState({teamData: teamCollection});
            console.log('Collection data is =======>' + JSON.stringify(teamCollection));
        }
    }


    async submitFinalData(){
        this.setState({isLoading:true});

        let selectedTeamData = await fetchFromLocal(SELECTED_TEAMS)
        let teamsData = await JSON.parse(selectedTeamData);
        let teamAId = teamsData[0].team_id;
        let teamBId = teamsData[1].team_id;

        let userDetails= await fetchFromLocal('loggedInUserDetails');
        userDetails = await JSON.parse(userDetails);
        let tournamentDetail = JSON.parse(this.tournamentInfo);

        let selectedDate = new Date(this.date);
        let tournamentDate= selectedDate.getFullYear() + '-' + (selectedDate.getMonth() +1) + '-' + selectedDate.getDate();

        let trnmentDetail = JSON.parse(this.tournamentInfo);

        let teamABehaviourKey = this.teamAName + ' Behaviour'
        let teamBBehaviourKey = this.teamBName + ' Behaviour'

        let teamAHCKey = this.teamAName + " Head Coach"
        let teamAACKey = this.teamAName + " Assistant Coach"
        let teamAFansKey = this.teamAName + " Fan"
        let teamAPlayersKey = this.teamAName + " Player"

        let teamBHCKey = this.teamBName + " Head Coach"
        let teamBACKey = this.teamBName + " Assistant Coach"
        let teamBFansKey = this.teamBName + " Fan"
        let teamBPlayersKey = this.teamBName + " Player"

        let improvementInfo = await this.startSurvey()

        console.log('@@@@@@@@@@@@@@@@@===========>' + JSON.stringify(improvementInfo));
        let finalSubmissionData = [];

        let submitdata = {};

        submitdata["Date"] = this.date;
        submitdata["TournamentName"] = trnmentDetail.tournament_name;
        submitdata["Teams"] = this.teamAName + ', ' + this.teamBName;

        let teamsPlayerData = [];
        let teamAPlayersInfo = {};
        let teamBPlayersInfo = {};

        let teamAOfficialSummString = 'TEAMA official summary';
        let teamARulesSummString = 'TEAMA rules summary';
        let teamAPlayersSummString = 'TEAMA players summary';
        let teamAOpponentSummString = 'TEAMA opponent summary';

        let teamBOfficialSummString = 'TEAMB official summary';
        let teamBRulesSummString = 'TEAMB rules summary';
        let teamBPlayersSummString = 'TEAMB players summary';
        let teamBOpponentSummString = 'TEAMB opponent summary';

        let teamAOfficialSummaryTemp = await fetchFromLocal(teamAOfficialSummString);
        console.log('=====================>');
        let teamARulesSummaryTemp = await fetchFromLocal(teamARulesSummString);
        console.log('=====================>');
        let teamAPlayersSummaryTemp = await fetchFromLocal(teamAPlayersSummString);
        console.log('=====================>');
        let teamAOpponentSummaryTemp = await fetchFromLocal(teamAOpponentSummString);

        console.log('=================>')
        let teamBOfficialSummaryTemp = await fetchFromLocal(teamBOfficialSummString);
        console.log('=====================>');
        let teamBRulesSummaryTemp = await fetchFromLocal(teamBRulesSummString);
        console.log('=====================>');
        let teamBPlayersSummaryTemp = await fetchFromLocal(teamBPlayersSummString);
        console.log('=====================>');
        let teamBOpponentSummaryTemp = await fetchFromLocal(teamBOpponentSummString);

        let teamAOfficialSummary;
        let teamARulesSummary;
        let teamAPlayersSummary;
        let teamAOpponentSummary;
        let teamBOfficialSummary;
        let teamBRulesSummary;
        let teamBPlayersSummary;
        let teamBOpponentSummary;

        let teamASummaryData = [];
        let teamBSummaryData = [];

        if (teamAOfficialSummaryTemp) {
            teamAOfficialSummary = JSON.parse(teamAOfficialSummaryTemp);
            teamASummaryData.push(teamAOfficialSummary);
        }

        if (teamARulesSummaryTemp) {
            teamARulesSummary = JSON.parse(teamARulesSummaryTemp);
            teamASummaryData.push(teamARulesSummary);
        }

        if (teamAPlayersSummaryTemp) {
            teamAPlayersSummary = JSON.parse(teamAPlayersSummaryTemp);
            teamASummaryData.push(teamAPlayersSummary);
        }

        if (teamAOpponentSummaryTemp) {
            teamAOpponentSummary = JSON.parse(teamAOpponentSummaryTemp);
            teamASummaryData.push(teamAOpponentSummary);
        }


        if (teamBOfficialSummaryTemp) {
            teamBOfficialSummary = JSON.parse(teamBOfficialSummaryTemp);
            teamBSummaryData.push(teamBOfficialSummary);
        }

        if (teamBRulesSummaryTemp) {
            teamBRulesSummary = JSON.parse(teamBRulesSummaryTemp);
            teamBSummaryData.push(teamBRulesSummary);
        }

        if (teamBPlayersSummaryTemp) {
            teamBPlayersSummary = JSON.parse(teamBPlayersSummaryTemp);
            teamBSummaryData.push(teamBPlayersSummary);
        }

        if (teamBOpponentSummaryTemp) {
            teamBOpponentSummary = JSON.parse(teamBOpponentSummaryTemp);
            teamBSummaryData.push(teamBOpponentSummary);
        }


        console.log('@@@@@@@@@@@@@@@@@@@@@@@@==========> TeamA Summary' + JSON.stringify(teamASummaryData));
        console.log('@@@@@@@@@@@@@@@@@@@@@@@@==========> TeamB Summary' + JSON.stringify(teamBSummaryData));

        //////////////////////////////////////////////////////////////////////////////////

        let teamAData = await fetchFromLocal('Team A improvement options');
        console.log('===========> The team A data is:::=>' + teamAData);
        let teamADataInfo = await JSON.parse(teamAData);
        let teamBData = await fetchFromLocal('Team B improvement options');
        console.log('===========> The team B data is:::=>' + teamBData);
        let teamBDataInfo = await JSON.parse(teamBData);
        console.log('===========> The team B data is:::=>' + teamBData);
        // submitdata[teamABehaviourKey] = this.behaviourAAnswer;
        // submitdata[teamBBehaviourKey] = this.behaviourBAnswer;


        let teamAQuestion = this.teamAName + ' behaviour'
        let teamBQuestion = this.teamBName + ' behaviour'

        let teamAReport = this.teamAName + ' Report'
        let teamBReport = this.teamBName + ' Report'

        let teamAAnswer = await fetchFromLocal(teamAQuestion)
        let teamBAnswer = await fetchFromLocal(teamBQuestion)

       if ((teamAAnswer === 'terrible') || (teamAAnswer === 'bad')) {
           teamAPlayersInfo[teamABehaviourKey] = this.behaviourAAnswer;
           teamAPlayersInfo[teamAHCKey] = this.AheadCoachInfo;
           teamAPlayersInfo[teamAACKey] = this.AassistantCoachInfo;
           teamAPlayersInfo[teamAFansKey] = this.AfansBehaviour;
           teamAPlayersInfo[teamAPlayersKey] = this.AplayersBehaviour;
           teamAPlayersInfo[teamAReport] = this.incidentReport;
           teamAPlayersInfo['summary'] = JSON.stringify(teamASummaryData);
        }

        if ((teamBAnswer === 'terrible') || (teamBAnswer === 'bad')) {
            teamBPlayersInfo[teamBBehaviourKey] = this.behaviourBAnswer;
            teamBPlayersInfo[teamBHCKey] = this.BheadCoachInfo;
            teamBPlayersInfo[teamBACKey] = this.BassistantCoachInfo;
            teamBPlayersInfo[teamBFansKey] = this.BfansBehaviour;
            teamBPlayersInfo[teamBPlayersKey] = this.BplayersBehaviour;
            teamBPlayersInfo[teamBReport] = this.incidentReport;
            teamBPlayersInfo['summary'] = JSON.stringify(teamBSummaryData);
        }


        if ((teamAAnswer === 'good') || (teamAAnswer === 'great') || (teamAAnswer === 'okay')) {
            teamAPlayersInfo[teamABehaviourKey] = this.behaviourAAnswer;
            teamAPlayersInfo[teamAHCKey] = this.behaviourAAnswer;
            teamAPlayersInfo[teamAACKey] = this.behaviourAAnswer;
            teamAPlayersInfo[teamAFansKey] = this.behaviourAAnswer;
            teamAPlayersInfo[teamAPlayersKey] = this.behaviourAAnswer;
            teamAPlayersInfo[teamAReport] = '';
            teamAPlayersInfo['summary'] = JSON.stringify(teamASummaryData);
        }


        if ((teamBAnswer === 'good') || (teamBAnswer === 'great') || (teamBAnswer === 'okay')) {
            teamBPlayersInfo[teamBBehaviourKey] = this.behaviourBAnswer;
            teamBPlayersInfo[teamBHCKey] = this.behaviourBAnswer;
            teamBPlayersInfo[teamBACKey] = this.behaviourBAnswer;
            teamBPlayersInfo[teamBFansKey] = this.behaviourBAnswer;
            teamBPlayersInfo[teamBPlayersKey] = this.behaviourBAnswer;
            teamBPlayersInfo[teamBReport] = '';
            teamBPlayersInfo['summary'] = JSON.stringify(teamBSummaryData);
        }


        teamsPlayerData = [...teamsPlayerData, teamAPlayersInfo, teamBPlayersInfo];

       console.log('=============================>' + JSON.stringify(submitdata));

       let reportCount = await fetchFromLocal(TAKE_REPORT_DATA);
       let feedbackRequired = reportCount ? JSON.parse(reportCount) : 0;
       if (feedbackRequired > 0) {
           submitdata["Incident Report"] = this.incidentReport;
       }

       console.log('=============================>' + JSON.stringify(submitdata));

       finalSubmissionData = [...finalSubmissionData, submitdata];
       if (improvementInfo.length > 0)
           finalSubmissionData = [...finalSubmissionData, improvementInfo];

       console.log('=======================> Final data to be submitted is       ' + JSON.stringify(finalSubmissionData) + teamAId, teamBId);

        let response = await finalSubmitData(userDetails.access_token, tournamentDetail.id, tournamentDate, teamAId, teamBId, userDetails.id, teamsPlayerData);
        if (response.success){
            this.setState({isLoading:false});
            let res = await resetLocalStorage()
            this.openAlert('Submitted successfully.');
        } else {
            this.setState({isLoading:false});
            if (response.message === INTERNET_CONNECTION_ISSUE){
                Alert.alert(APP_DISPLAY_NAME,INTERNET_CONNECTION_ISSUE)
            } else {
                showInfoAlert(response.data.message)
            }
        }
    }

    goToHome() {

        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Home' })],
        });
        this.props.navigation.dispatch(resetAction);
    }

    openAlert(message){
        Alert.alert(
            APP_DISPLAY_NAME,
            message,
            [
                {text: 'Ok', onPress: () => this.goToHome()}
            ],
            {cancelable: false},
        )
    }

    render() {
        const upperViewHeight = deviceHeight / 3;//2 - 160;
        return (
            <View style={{flex: 1, backgroundColor: 'white'}}>
                {this.state.isLoading && <View style={{position:'absolute', top:0, bottom:0, zIndex:222, left:0, width:'100%', height:deviceHeight, flex:1, justifyContent:'center', alignItems:'center', backgroundColor:'black', opacity:0.5}}>
                    <ActivityIndicator size="large" color="white" /></View>
                }
                <View
                    style={{
                        width: deviceWidth,
                        height: upperViewHeight,
                        backgroundColor: '#0E0F3F',
                    }}>
                    <SurveyTopBar step={5} navigation={this.props.navigation} />
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            marginLeft: 10,
                            height: 100,
                            width: deviceWidth,
                        }}>
                        <Text
                            style={{
                                color: 'white',
                                width: deviceWidth - 40,
                                fontSize: 38,
                                fontWeight: 'bold',
                            }}>
                            Please review your answers before submitting:
                        </Text>
                    </View>
                </View>
                <View
                    style={{
                        width: deviceWidth,
                        height: deviceHeight * (2/3), //+ 160,
                        backgroundColor: '#0E0F3F',
                    }}>
                    <ScrollView>
                    <FlatList
                        data={this.state.teamData}
                        renderItem={({item, index}) => (
                            <View
                                style={{
                                    alignItems: 'flex-start',
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    flex: 1,
                                    height: 60,
                                    margin: 5,
                                    marginLeft: 20,
                                    marginRight: 20,
                                    backgroundColor: 'white',
                                    borderRadius: 30,
                                }}>
                                <View style={{alignItems:'center', flexDirection:"row", height:60,borderBottomLeftRadius:30, borderTopLeftRadius:30}}>
                                <Text style={{marginLeft:15, paddingHorizontal: 2, fontSize:16, color:'#7C808E', width:deviceWidth-120}}>{item.name}<Text style={{marginLeft:10, color:'black', fontWeight:'600'}}>{' ' + item.value}</Text></Text>
                                </View>
                                <Image
                                    style={{height: 40, width: 40, marginTop:10, marginRight:14}}
                                    source={item.image}
                                />
                                <TouchableOpacity style={{position:'absolute', width: deviceWidth-60, height:60}}>
                                </TouchableOpacity>
                            </View>
                        )}
                        numColumns={1}
                    />
                        <TouchableOpacity style={{height:50, width:200, backgroundColor:'red', marginLeft:deviceWidth/2 - 100, alignItems:'center', justifyContent:'center', borderRadius:25, marginBottom:50}} onPress={()=>this.submitFinalData()}>
                            <Text style={{color:'white', alignItems:'center', justifyContent:'center', fontSize:16, fontWeight:'600'}}>Submit</Text>
                        </TouchableOpacity>
                    </ScrollView>
                </View>
            </View>
        );
    }
}
