import React, {Component} from 'react';
import {
    AppRegistry,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    Image,
    FlatList,
    ScrollView
} from 'react-native';
import SurveyTopBar from '../../Components/SurveyTopBar';
import SurveySmiles from "../../Components/SurveySmiles";
import {fetchFromLocal} from "../../Utils/SaveDetails";
import {SELECTED_TEAMS} from "../../Constants/StringConstants";

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;

export default class TeamExperience extends Component {
    constructor(props) {
        super(props);
        this.state = {
            teamAName:'',
            teamBName:'',
            isActive:1,
        };
    }

    async componentDidMount(){
        let selectedTeamData = await fetchFromLocal(SELECTED_TEAMS)
        let teamsData = await JSON.parse(selectedTeamData);
        this.setState({teamAName:teamsData[0].team_name + " behaviour"})
        this.setState({teamBName:teamsData[1].team_name + " behaviour"})
    }

    active(v){
        this.setState({
            isActive : v
        })
    }
    render() {
        const upperViewHeight = deviceHeight / 2 - 50;
        return (
            <View style={{flex: 1, backgroundColor: 'white'}}>
                <View
                    style={{
                        width: deviceWidth,
                        height: upperViewHeight,
                        backgroundColor: '#0E0F3F',
                    }}>
                    <SurveyTopBar step={4} navigation={this.props.navigation} fromScreen="TeamExperience" questionOne={this.state.teamAName} questionSecond={this.state.teamBName}/>
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            marginLeft: 10,
                            height: 100,
                            width: deviceWidth,
                        }}>
                        <Text
                            style={{
                                color: 'white',
                                width: deviceWidth - 40,
                                fontSize: 33,
                                fontWeight: 'bold',
                            }}>
                            How was the behaviour of the players, coaches and fans?
                        </Text>
                    </View>
                </View>
                <View style={{width:deviceWidth, height:10, backgroundColor:'#E93D1B'}}>
                </View>
                <ScrollView>
                <View
                    style={{
                        width: deviceWidth,
                        height: deviceHeight / 2 + 160,
                        backgroundColor: '#F6F7FB',
                    }}>
                    <SurveySmiles
                        answerText=''
                        activeCurrect={this.active.bind(this)}
                        currectActive={1}
                        isActive={this.state.isActive}
                        question={this.state.teamAName}
                        allQuestions=""
                        navigation={this.props.navigation}
                        toMoveScreen=""
                        questionNumber={1}
                        currentScreen="TeamExperience"/>
                    <SurveySmiles
                        answerText=''
                        activeCurrect={this.active.bind(this)}
                        currectActive={2}
                        isActive={this.state.isActive}
                        question={this.state.teamBName}
                        allQuestions={this.state.teamAName}
                        navigation={this.props.navigation}
                        toMoveScreen="MoreTeamABehaviour"
                        finalScreen="FinalSubmission"
                        questionNumber={2}
                        currentScreen="TeamExperience"/>
                </View>
                </ScrollView>
            </View>
        )
    }


}
