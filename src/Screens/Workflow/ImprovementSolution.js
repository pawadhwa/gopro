import React, {Component} from 'react';
import {
    AppRegistry,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    Image,
    FlatList,
} from 'react-native';
import SurveyTopBar from '../../Components/SurveyTopBar';
import SurveySmiles from "../../Components/SurveySmiles";
import {fetchFromLocal, saveToLocal} from "../../Utils/SaveDetails";
import {IMPROVEMENT_SOLUTION, SELECTED_TEAMS} from "../../Constants/StringConstants";

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;

export default class ImprovementSolution extends Component {

    constructor(props) {
        super(props);
        this.state = {
            headerQuestion:'',
            possibleAnswers:[],
            officialImprovementSolution: [
                {name: 'Yelling at official.'},
                {name: 'Bad body language toward official.'},
                {name: 'Making verbal threats to official'},
                {name: 'Getting physical with official'},
                {name: 'Did you not feel safe at any time?'},
            ],
            gameImprovementSolution: [
                {name: 'Bad game spirit.'},
                {name: 'Bad body language during game.'},
                {name: 'Making verbal threats during game'},
                {name: 'Did you not feel safe at any time?'},
            ],
            playersImprovementSolution: [
                {name: 'Yelling at players.'},
                {name: 'Bad body language toward players.'},
                {name: 'Making verbal threats to players'},
                {name: 'Getting physical with players'},
                {name: 'Did you not feel safe at any time?'},
            ],
            rulesImprovementSolution: [
                {name: 'Not following rules'},
                {name: 'Bad body language toward official.'},
                {name: 'Making verbal threats to official'},
                {name: 'Getting physical with official'},
                {name: 'Did you not feel safe at any time?'},
            ],
            opponentImprovementSolution: [
                {name: 'Yelling at opponents.'},
                {name: 'Bad body language toward opponents.'},
                {name: 'Making verbal threats to opponents'},
                {name: 'Getting physical with opponents'},
                {name: 'Did you not feel safe at any time?'},
            ],
            selectedNumber:0,
            teamName:'',
        };
    }

    async componentDidMount(){
        let category = await fetchFromLocal(IMPROVEMENT_SECTION)


        let selectedTeamData = await fetchFromLocal(SELECTED_TEAMS)
        let teamsData = await JSON.parse(selectedTeamData);
        let teamAName = teamsData[0].team_name + " behaviour";
        let teamBName = teamsData[1].team_name + " behaviour";
        //if (teamAName === 'bad' || teamAName === 'terrible') {
        this.setState({teamName: teamsData[0].team_name})

        if (category === 'Respect the Game') {
            this.setState({headerQuestion:'How did they not respect the game?'})
            this.setState({possibleAnswers:this.state.gameImprovementSolution})
        }
        else if (category === 'Respect the Players') {
            this.setState({headerQuestion:'How did they not respect the players?'})
            this.setState({possibleAnswers:this.state.playersImprovementSolution})
        }
        else if (category === 'Respect the Rules') {
            this.setState({headerQuestion:'How did they not respect the rules?'})
            this.setState({possibleAnswers:this.state.rulesImprovementSolution})
        }
        else if (category === 'Respect the Opponent') {
            this.setState({headerQuestion:'How did they not respect the opponent?'})
            this.setState({possibleAnswers:this.state.opponentImprovementSolution})
        }
        else if (category === 'Respect the Official') {
            this.setState({headerQuestion:'How did they not respect the official?'})
            this.setState({possibleAnswers:this.state.officialImprovementSolution})
        }
    }


    submitImprovementReport(index){

        let teamCollection = this.state.possibleAnswers;
        let selectedTeamNumber = this.state.selectedNumber;
        if(this.state.selectedNumber < 1){
            let teamInfo = teamCollection[index];
            teamInfo.selected = !teamInfo.selected;
            teamCollection[index] = teamInfo;
            selectedTeamNumber = selectedTeamNumber + 1;
            this.setState({selectedNumber:selectedTeamNumber});
            let that = this;
            if (selectedTeamNumber === 1) {
                setTimeout(async function () {
                    await saveToLocal(IMPROVEMENT_SOLUTION,teamInfo.name)
                    that.props.navigation.navigate('FeedbackScreen');
                }, 1000);
            }
        }
        else {
            let teamInfo = teamCollection[index];
            if (teamInfo.selected) {
                teamInfo.selected = !teamInfo.selected;
                selectedTeamNumber = selectedTeamNumber - 1;
                this.setState({selectedNumber:selectedTeamNumber});
            }
            teamCollection[index] = teamInfo;
        }
        this.setState({teamImprovementData:teamCollection});
    }

    render() {
        const upperViewHeight = deviceHeight / 3;//2 - 150;
        return (
            <View style={{flex:1, backgroundColor:'white'}}>
                <View
                    style={{
                        width: deviceWidth,
                        height: upperViewHeight,
                        backgroundColor: '#0E0F3F',
                    }}>
                    <SurveyTopBar step={5} navigation={this.props.navigation}/>
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            marginLeft: 10,
                            height: 100,
                            width: deviceWidth,
                        }}>
                        <Text
                            style={{
                                color: 'white',
                                width: deviceWidth - 40,
                                fontSize: 33,
                                fontWeight: 'bold',
                            }}>
                            {this.state.headerQuestion}
                        </Text>
                    </View>
                    <View style={{width:deviceWidth, height:10, backgroundColor:'#E93D1B'}}/>
                </View>
                <View
                    style={{
                        width: deviceWidth,
                        height: deviceHeight * (2/3), //+ 150,
                        backgroundColor: '#F6F7FB',
                    }}>
                    <FlatList
                        data={this.state.possibleAnswers}
                        renderItem={({item, index}) => (
                            <View
                                style={{
                                    alignItems: 'flex-start',
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    flex: 1,
                                    height: 60,
                                    margin: 5,
                                    marginLeft: 20,
                                    marginRight: 20,
                                    backgroundColor: 'white',
                                    borderRadius: 30,
                                }}>
                                <Text style={{marginTop: 20, marginLeft: 15, fontSize:16}}>{item.name}</Text>
                                <Image
                                    style={{height: 40, width: 40, marginTop:10, marginRight:14}}
                                    source={item.selected ? require('../../Images/Icons/Selected.png') : require('../../Images/Icons/Add.png')}
                                />
                                <TouchableOpacity style={{position:'absolute', width: deviceWidth-60, height:60}} onPress={()=>this.submitImprovementReport(index)}>
                                </TouchableOpacity>
                            </View>
                        )}
                    />
                </View>
            </View>
        );
    }
}
