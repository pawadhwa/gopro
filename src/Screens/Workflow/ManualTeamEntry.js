import React, {Component} from 'react';
import {
    AppRegistry,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    TextInput,
    Alert,
    TouchableHighlight, Platform
} from 'react-native';
import SurveyTopBar from "../../Components/SurveyTopBar";
import index from "react-native-fetch-blob";
import {saveToLocal} from "../../Utils/SaveDetails";
import {AGE_GROUP, GAME_TYPE, SELECTED_TEAMS} from "../../Constants/StringConstants";
import QuickPicker from "quick-picker";

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;

const paddingValue = Platform.isPad ? true : false;

export default class ManualTeamEntry extends Component {
    constructor(props) {
        super(props);
        this.state = {
            signUpSelected: true,
            loginSelected: false,
            selectedNumber: 0,
            selectedAge: '',
            teamAName: '',
            teamBName: ''
        };
    }

    async moveToTeamDetails() {
        if ((this.state.teamAName !== '') && (this.state.teamBName !== '')) {

            let teamsData = [
                {
                    "team_id": -1,
                    "team_name": this.state.teamAName,
                },
                {
                    "team_id": -2,
                    "team_name": this.state.teamBName,
                }
            ];
            await saveToLocal(SELECTED_TEAMS,JSON.stringify(teamsData))
            this.props.navigation.navigate('TeamExperience');
        }
    }


    render() {
        const upperViewHeight = deviceHeight / 3 - 40;
        return (
            <View>
                <View
                    style={{
                        width: deviceWidth,
                        height: upperViewHeight,
                        backgroundColor: '#0E0F3F',
                    }}>
                    <SurveyTopBar step={3} navigation={this.props.navigation}/>
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            marginLeft: 10,
                            height: 100,
                            width: deviceWidth,
                        }}>
                        <Text
                            style={{
                                color: 'white',
                                width: deviceWidth - 40,
                                fontSize: 38,
                                fontWeight: 'bold',
                            }}>
                            Enter Team Names.
                        </Text>
                    </View>
                </View>
                <View
                    style={{
                        width: deviceWidth,
                        height: deviceHeight * (2/3) + 20,//+ 150,
                        backgroundColor: '#F6F7FB',
                    }}>
                    <TextInput
                        onChangeText={text => this.setState({teamAName: text})}
                        underlineColorAndroid="transparent"
                        autoCapitalize="none"
                        placeholder="Enter team A name"
                        style={{
                            textAlign: 'left',
                            width: deviceWidth - (paddingValue ? 280 : 80),
                            marginLeft: paddingValue ? 140 : 40,
                            marginBottom: 7,
                            marginTop:20,
                            height: Platform.OS === 'ios' ? 40 : 45,
                            borderRadius: 5,
                            fontSize: 18,
                            color: 'black',
                            paddingHorizontal: 5,
                            borderWidth:0.5,
                            borderBottomColor: '#808FAE',
                        }}
                    />
                    <TextInput
                        onChangeText={text => this.setState({teamBName: text})}
                        underlineColorAndroid="transparent"
                        autoCapitalize="none"
                        placeholder="Enter team B name"
                        style={{
                            textAlign: 'left',
                            width: deviceWidth - (paddingValue ? 280 : 80),
                            marginLeft: paddingValue ? 140 : 40,
                            marginBottom: 7,
                            marginTop:20,
                            height: Platform.OS === 'ios' ? 40 : 45,
                            borderRadius: 5,
                            fontSize: 18,
                            color: 'black',
                            paddingHorizontal: 5,
                            borderWidth:0.5,
                            borderBottomColor: '#808FAE',
                        }}
                    />
                    <TouchableOpacity
                        style={{height:50,width:160, backgroundColor:'#E93D1B', marginBottom:30, alignItems:'center', justifyContent:'center', marginLeft:deviceWidth/2 - 80, borderRadius:25, marginTop:10}}
                        onPress={()=>this.moveToTeamDetails()}>
                        <Text style={{color:'white',fontSize:18, fontWeight:'600'}}>Next</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
