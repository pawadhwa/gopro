import React, {Component} from 'react';
import {
    AppRegistry,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    Image,
    ScrollView,
    ImageBackground,
    Platform,
    FlatList,
    StyleSheet,
    Alert, ActivityIndicator,
} from 'react-native';

import {
    INTERNET_CONNECTION_ISSUE,
    APP_DISPLAY_NAME,
} from '../../Constants/StringConstants'


import SurveyTopBar from '../../Components/SurveyTopBar';
import {fetchFromLocal, saveTournamentDetails} from "../../Utils/SaveDetails";
import {getTournamentList} from "../Navigation/Networking";
import {showInfoAlert} from "../../Utils/AlertPopUp";

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;
const paddingValue = Platform.isPad ? true : false;

export default class Welcome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      tournamentData: [],
    };
  }

  GetGridViewItem(item) {
    Alert.alert(item);
  }

  fetchTeamDetails(selectedIndex, tournament) {
      let selectedTournament = this.state.tournamentData[selectedIndex];
      saveTournamentDetails(JSON.stringify(selectedTournament)).then(r => {
          this.props.navigation.navigate('TournamentSchedule', tournament);
      });
  }

  async componentDidMount() {

      this.setState({isLoading:true});

      let userDetails= await fetchFromLocal('loggedInUserDetails');
      userDetails = await JSON.parse(userDetails);
      console.log(JSON.stringify(userDetails));
      let response = await getTournamentList(userDetails.access_token);
      if (response.success)
      {
          this.setState({isLoading:false})
          this.setState({tournamentData: response.data})
      }
      else {
          this.setState({isLoading:false})
          if (response.message === INTERNET_CONNECTION_ISSUE){
              Alert.alert(APP_DISPLAY_NAME,'Connect to the network.')
          } else {
              showInfoAlert(response.message)
          }
      }
  }

  render() {
        const upperViewHeight = deviceHeight / 3; //2 - 160;
    const imageSize = deviceWidth / 3 + 50;
    return (
      <View style={{flex: 1}}>
          {this.state.isLoading && <View style={{position:'absolute', top:0, bottom:0, zIndex:222, left:0, width:'100%', height:deviceHeight, flex:1, justifyContent:'center', alignItems:'center', backgroundColor:'black', opacity:0.5}}>
              <ActivityIndicator size="large" color="white" /></View>
          }
        <View
          style={{
            flex: 1,
            width: deviceWidth,
            height: upperViewHeight,
            backgroundColor: '#0E0F3F',
          }}>
          <SurveyTopBar step={1} navigation={this.props.navigation} />
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              marginLeft: 10,
              height: 100,
              width: deviceWidth,
            }}>
            <Text
              style={{
                color: 'white',
                // width:deviceWidth-30,
                fontSize: 40,
                fontWeight: 'bold',
              }}>
              Select your Tournament
            </Text>
          </View>
        </View>
          <View style={{width:deviceWidth, height:10, backgroundColor:'#E93D1B'}}>
          </View>
        <View
          style={{
            width: deviceWidth,
            height: deviceHeight * (2/3),
            backgroundColor: '#F6F7FB',
          }}>
          <FlatList
            data={this.state.tournamentData}
            renderItem={({item, index}) => (
              <View style={styles.GridViewBlockStyle}>
                <Image
                  style={{width: deviceWidth / 2 - 80, height: 130}}
                  source={{uri: item.tournament_logo ? item.tournament_logo : ""}}
                />
                <Text
                  style={styles.GridViewInsideTextItemStyle}
                  onPress={this.GetGridViewItem.bind(this, item.key)}>
                  {' '}
                  {item.tournament_name}{' '}
                </Text>
                <TouchableOpacity
                  style={{
                    backgroundColor: 'clear',
                    position: 'absolute',
                    width: '100%',
                    height: 180,
                  }}
                  onPress={() => this.fetchTeamDetails(index, item)}
                />
              </View>
            )}
            numColumns={2}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    justifyContent: 'center',
    flex: 1,
    margin: 10,
    paddingTop: Platform.OS === 'ios' ? 20 : 0,
  },

  GridViewBlockStyle: {
    alignItems: 'center',
    height: 180,
      width:deviceWidth/2 - 10,
    // padding: 5,
    margin: 5,
    backgroundColor: 'white',
  },
  GridViewInsideTextItemStyle: {
    color: 'black',
    marginTop: 5,
    fontSize: 16,
  },
});
