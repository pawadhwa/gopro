import React, { Component } from 'react';
import {
    AsyncStorage,
    Dimensions, Platform,
    StyleSheet,
    Text,
    View, Alert
} from 'react-native';
import CalendarPicker from 'react-native-calendar-picker';
import SurveyTopBar from "../../Components/SurveyTopBar";
import {GAME_TYPE, TOURNAMENT_DATE, TOURNAMENT_DETAILS} from "../../Constants/StringConstants";
import {fetchFromLocal} from "../../Utils/SaveDetails";
let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;
const paddingValue = Platform.isPad ? true : false;

export default class TournamentSchedule extends Component {


    constructor(props) {
        super(props);
        this.state={
            tournamentId:'',
            tournamentDate:'',
            tournamentType:''
        }
        this.onDateChange = this.onDateChange.bind(this);
    }

    async componentDidMount(){

        let gameTypeData = await fetchFromLocal(GAME_TYPE)
        let gameType = JSON.parse(gameTypeData);
        this.setState({tournamentType:gameType[0].name});

        let tournamentDetail= this.props.navigation.getParam('id');
        this.setState({tournamentId:tournamentDetail});
    }

    formatDate(date) {
        var monthNames = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];
        let day = date.getDate();
        let monthIndex = date.getMonth();
        let year = date.getFullYear();

        return monthNames[monthIndex]  + ' ' +day + ',' + ' ' + year;
    }


    async onDateChange(date) {

        let selectedDate = this.formatDate(new Date(date))
        await AsyncStorage.setItem(TOURNAMENT_DATE, selectedDate, () => {
            console.log("Tournament date saved is====>" + selectedDate);
            selectedDate = new Date(date);
            let tournamentDate= selectedDate.getFullYear() + '-' + (selectedDate.getMonth() +1) + '-' + selectedDate.getDate();
            this.setState({tournamentDate: tournamentDate});
        });

        let tournamentId = this.state.tournamentId;
        let tournamentDate = this.state.tournamentDate;

        let that = this;
        setTimeout(function () {
            if (that.state.tournamentType === 'Tournament') {
                that.props.navigation.navigate('TeamInfo',{id: tournamentId, date: tournamentDate})
            } else {
                that.props.navigation.navigate('SelectGender')
            }
        }, 1000);
    }


    render() {
        const upperViewHeight = deviceHeight / 3; //2 - 160;
        return (
            <View style={{flex: 1}}>
                <View
                    style={{
                        flex: 1,
                        width: deviceWidth,
                        height: upperViewHeight,
                        backgroundColor: '#0E0F3F',
                    }}>
                    <SurveyTopBar step={2} navigation={this.props.navigation} />
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            marginLeft: 10,
                            height: 100,
                            width: deviceWidth,
                        }}>
                        <Text
                            style={{
                                color: 'white',
                                fontSize: 40,
                                fontWeight: 'bold',
                            }}>
                            Select the date of the game.
                        </Text>
                    </View>
                </View>
                <View style={{width:deviceWidth, height:10, backgroundColor:'#E93D1B'}}>
                </View>
                <View
                    style={{
                        width: deviceWidth,
                        height: deviceHeight * (2/3), //2 + 160,
                        backgroundColor: '#F6F7FB',
                    }}>
                        <CalendarPicker
                            onDateChange={this.onDateChange}
                        />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        marginTop: 100,
    },
});

