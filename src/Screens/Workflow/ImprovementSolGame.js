import React, {Component} from 'react';
import {
    AppRegistry,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    Image,
    FlatList,
} from 'react-native';
import SurveyTopBar from '../../Components/SurveyTopBar';
import SurveySmiles from "../../Components/SurveySmiles";
import {fetchFromLocal, removeFromLocal, saveToLocal} from "../../Utils/SaveDetails";
import {
    IMPROVEMENT_SOLUTION,
    SELECTED_TEAMS,
    TEAMA_SURVEY_DONE,
    TEAMB_SURVEY_DONE,
    TEAMA_COACH_IMPROVEMENT_REQUIRED,
    TEAMA_ASTTCOACH_IMPROVEMENT_REQUIRED,
    TEAMA_FANS_IMPROVEMENT_REQUIRED, TEAMA_PLAYERS_IMPROVEMENT_REQUIRED, TAKE_REPORT_DATA
} from "../../Constants/StringConstants";

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;

export default class ImprovementSolGame extends Component {

    constructor(props) {
        super(props);
        this.state = {
            headerQuestion:'',
            possibleAnswers:[],
            rulesImprovementSolution: [
                {name: 'Throwing equipment'},
                {name: 'Obvious lack of effort'},
                {name: 'I need to file a report'},
            ],
            selectedNumber:0,
            teamName:'',
            isBBehaviourRequired:false,
            isTeamA:false,
            reportChecked:false
        };
    }

    async componentDidMount(){

        let selectedTeamData = await fetchFromLocal(SELECTED_TEAMS)
        let teamsData = await JSON.parse(selectedTeamData);

        if (this.props.navigation.state.key === 'ImprovementSolGame'){
            this.setState({teamName: teamsData[0].team_name})
            this.setState({isTeamA: true})
        } else {
            this.setState({teamName: teamsData[1].team_name})
            this.setState({isTeamA: false})
        }

        this.setState({headerQuestion:'How did they not respect the game?'})
        this.setState({possibleAnswers:this.state.gameImprovementSolution})


        //Fetch team B feedback status to be asked or not
        let questionB = teamsData[1].team_name + " behaviour"
        let teamBAnswer = await fetchFromLocal(questionB);

        if ((teamBAnswer === 'terrible') || (teamBAnswer === 'bad')) {
            this.setState({isBBehaviourRequired:true})
        } else {
            this.setState({isBBehaviourRequired:false})
        }
    }

    async toGoNext(){

        console.log('===============> Btn Action called')
        if (this.state.selectedNumber === 0)
            return;
        let teamString = this.state.isTeamA ? 'TEAMA' : 'TEAMB';

        let playerQuestString = teamString + '_RESPECT_PLAYERS_REQUIRED';
        let teamAPlayersRequired = await fetchFromLocal(playerQuestString);
        let teamAPlayersFeedbackRequired = JSON.parse(teamAPlayersRequired);
        console.log('######################')

        let rulesQuestString = teamString + '_RESPECT_RULES_REQUIRED';
        let teamARulesRequired = await fetchFromLocal(rulesQuestString);
        let teamARulesFeedbackRequired = JSON.parse(teamARulesRequired);
        console.log('######################')

        let opponentQuestString = teamString + '_RESPECT_OPPONENT_REQUIRED';
        let teamAOpponentRequired = await fetchFromLocal(opponentQuestString);
        let teamAOpponentFeedbackRequired = JSON.parse(teamAOpponentRequired);
        console.log('######################')

        let officialQuestString = teamString + '_RESPECT_OFFICIAL_REQUIRED';
        let teamAOfficialRequired = await fetchFromLocal(officialQuestString);
        let teamAOfficialFeedbackRequired = JSON.parse(teamAOfficialRequired);
        console.log('######################')


        if (teamAPlayersFeedbackRequired) {
            let navigationString = this.state.isTeamA ? 'ImprovementSolPlayers' : 'ImprovementSolPlayersA'
            this.props.navigation.navigate({
                key:navigationString,
                routeName:navigationString
            })
        } else if (teamARulesFeedbackRequired) {
            let navigationString = this.state.isTeamA ? 'ImprovementSolRules' : 'ImprovementSolRulesA';
            this.props.navigation.navigate({
                key:navigationString,
                routeName:navigationString
            })
        } else if (teamAOpponentFeedbackRequired) {
            let navigationString = this.state.isTeamA ? 'ImprovementSolOpponent' : 'ImprovementSolOpponentA';
            this.props.navigation.navigate({
                key:navigationString,
                routeName:navigationString
            })
        } else if (teamAOfficialFeedbackRequired) {
            let navigationString = this.state.isTeamA ? 'ImprovementSolOfficials' : 'ImprovementSolOfficialsA';
            this.props.navigation.navigate({
                key:navigationString,
                routeName:navigationString
            })
        }
        else {
            if (this.state.isTeamA) {
                if (this.state.isBBehaviourRequired) {
                    this.props.navigation.navigate({
                        key: 'MoreTeamBBehaviour',
                        routeName: 'MoreTeamBBehaviour'
                    })
                } else {
                    let reportCount = await fetchFromLocal(TAKE_REPORT_DATA);
                    let feedbackRequired = reportCount ? JSON.parse(reportCount) : 0;
                    if (feedbackRequired) {
                        if (feedbackRequired > 0) {
                            this.props.navigation.navigate({
                                key: 'FeedbackScreen',
                                routeName: 'FeedbackScreen'
                            })
                        } else {
                            this.props.navigation.navigate({
                                key: 'FinalSubmission',
                                routeName: 'FinalSubmission'
                            })
                        }
                    }
                    else {
                        this.props.navigation.navigate({
                            key: 'FinalSubmission',
                            routeName: 'FinalSubmission'
                        })
                    }
                }
            } else {
                let reportCount = await fetchFromLocal(TAKE_REPORT_DATA);
                let feedbackRequired = reportCount ? JSON.parse(reportCount) : 0;
                if (feedbackRequired) {
                    if (feedbackRequired > 0) {
                        this.props.navigation.navigate({
                            key: 'FeedbackScreen',
                            routeName: 'FeedbackScreen'
                        })
                    } else {
                        this.props.navigation.navigate({
                            key: 'FinalSubmission',
                            routeName: 'FinalSubmission'
                        })
                    }
                }
                else {
                    this.props.navigation.navigate({
                        key: 'FinalSubmission',
                        routeName: 'FinalSubmission'
                    })
                }
            }
        }
    }

    async submitImprovementReport(index){

        let teamCollection = this.state.rulesImprovementSolution;
        let selectedTeamNumber = this.state.selectedNumber;
        let teamInfo = teamCollection[index];

        if (teamInfo.selected) {
            teamInfo.selected = !teamInfo.selected;
            selectedTeamNumber = selectedTeamNumber - 1;
            this.setState({selectedNumber:selectedTeamNumber});
            teamCollection[index] = teamInfo;
        }
        else {
            let teamInfo = teamCollection[index];
            teamInfo.selected = !teamInfo.selected;
            teamCollection[index] = teamInfo;
            selectedTeamNumber = selectedTeamNumber + 1;
            this.setState({selectedNumber:selectedTeamNumber});
        }


        //SAVE FOR REPORT INPUT AT END OF WORK_FLOW
        if (index === 2) {
            let reportData = await fetchFromLocal(TAKE_REPORT_DATA);
            let reportCount = reportData ? JSON.parse(reportData) : 0;
            if (teamInfo.selected) {
                await saveToLocal(TAKE_REPORT_DATA,JSON.stringify(reportCount+1))
                this.setState({reportChecked:true});
            } else {
                await saveToLocal(TAKE_REPORT_DATA,JSON.stringify(reportCount-1))
                this.setState({reportChecked:false});
            }
        }


        this.setState({teamImprovementData:teamCollection}, async ()=>{
            let selectedData = [];
            let metaData = this.state.teamImprovementData;
            for(let i = 0; i < metaData.length; i++){
                let teamInfo = this.state.teamImprovementData[i];
                if (teamInfo.selected)
                    selectedData = [...selectedData, teamInfo]
            }

            let stringifiedData = JSON.stringify(selectedData);
            console.log('The selected players options are ' + JSON.stringify(selectedData));

            let saveGameKey = this.state.teamName + '_GAME_SUGGESTION'
            await saveToLocal(saveGameKey,stringifiedData);
        });
    }

    render() {
        const upperViewHeight = deviceHeight / 3;//2 - 150;
        return (
            <View style={{flex:1, backgroundColor:'white'}}>
                <View
                    style={{
                        width: deviceWidth,
                        height: upperViewHeight,
                        backgroundColor: '#0E0F3F',
                    }}>
                    <SurveyTopBar
                        step={4}
                        navigation={this.props.navigation}
                        reportChecked={this.state.reportChecked}/>
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            marginLeft: 10,
                            height: 100,
                            width: deviceWidth,
                        }}>
                        <Text
                            style={{
                                color: 'white',
                                width: deviceWidth - 40,
                                fontSize: 33,
                                fontWeight: 'bold',
                            }}>
                            {this.state.headerQuestion}
                        </Text>
                    </View>
                    <View style={{width:deviceWidth, height:10, backgroundColor:'#E93D1B'}}/>
                </View>
                <View
                    style={{
                        width: deviceWidth,
                        height: deviceHeight * (2/3), //+ 150,
                        backgroundColor: '#F6F7FB',
                    }}>
                    <FlatList
                        data={this.state.rulesImprovementSolution}
                        renderItem={({item, index}) => (
                            <View
                                style={{
                                    alignItems: 'flex-start',
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    flex: 1,
                                    height: 60,
                                    margin: 5,
                                    marginLeft: 20,
                                    marginRight: 20,
                                    backgroundColor: 'white',
                                    borderRadius: 30,
                                }}>
                                <Text style={{marginTop: 20, marginLeft: 15, fontSize:16}}>{item.name}</Text>
                                <Image
                                    style={{height: 40, width: 40, marginTop:10, marginRight:14}}
                                    source={item.selected ? require('../../Images/Icons/Selected.png') : require('../../Images/Icons/Add.png')}
                                />
                                <TouchableOpacity style={{position:'absolute', width: deviceWidth-60, height:60}} onPress={()=>this.submitImprovementReport(index)}>
                                </TouchableOpacity>
                            </View>
                        )}
                    />
                    <TouchableOpacity
                        style={{height:50,width:160, backgroundColor:'#E93D1B', marginBottom:30, alignItems:'center', justifyContent:'center', marginLeft:deviceWidth/2 - 80, borderRadius:25}}
                        onPress={()=>this.toGoNext()}>
                        <Text style={{color:'white',fontSize:18, fontWeight:'600'}}>Next</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
