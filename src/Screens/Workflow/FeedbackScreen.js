import React, {Component} from 'react';
import {
    AppRegistry,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    Image,
    FlatList,
    TextInput, Platform,
    Alert
} from 'react-native';
import SurveyTopBar from '../../Components/SurveyTopBar';
import SurveySmiles from "../../Components/SurveySmiles";
import {INCIDENT_REPORT} from "../../Constants/StringConstants";
import {saveToLocal} from "../../Utils/SaveDetails";

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;
const paddingValue = Platform.isPad ? true : false;

export default class FeedbackScreen extends Component {

    constructor(props){
        super(props);
        this.state = {
            feedbackText:''
        }
    }

    async goToFinalSubmission(e){
        await saveToLocal(INCIDENT_REPORT,this.state.feedbackText);
        this.props.navigation.navigate('FinalSubmission');
    }


    render() {
        const upperViewHeight = deviceHeight / 3; //2 - 160;
        return (
            <View style={{flex: 1, backgroundColor: 'white'}}>
                <View
                    style={{
                        width: deviceWidth,
                        height: upperViewHeight,
                        backgroundColor: '#0E0F3F',
                    }}>
                    <SurveyTopBar step={5} navigation={this.props.navigation} />
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            marginLeft: 10,
                            height: 100,
                            width: deviceWidth,
                        }}>
                        <Text
                            style={{
                                color: 'white',
                                width: deviceWidth - 40,
                                fontSize: 38,
                                fontWeight: 'bold',
                            }}>
                            You reported a zero-tolerance incident.
                        </Text>
                    </View>
                    <View style={{width:deviceWidth, height:10, backgroundColor:'#E93D1B'}}/>
                </View>
                <View
                    style={{
                        width: deviceWidth,
                        height: deviceHeight * (2/3), //+ 160,
                        backgroundColor: '#F6F7FB',
                    }}>
                    <View style={{marginTop:20, marginLeft:20, marginRight:20}}>
                        <Text>
                            We need to ensure that this is handled in a swift manner. Can you supply the details of the event?
                        </Text>
                        <TextInput
                            onChangeText={text => this.setState({feedbackText: text})}
                            placeholder="Report details here..."
                            multiline={true}
                            onSubmitEditing={(event) => this.goToFinalSubmission(event)}
                            blurOnSubmit = {true}
                            returnKeyType="done"
                            style={{
                                textAlign: 'left',
                                backgroundColor:'white',
                                marginBottom: 7,
                                height: deviceHeight / 2 -50,
                                borderRadius: 5,
                                fontSize: 15,
                                marginTop:10,
                                paddingHorizontal: 5,
                                textAlignVertical: "top",
                            }}
                        />
                    </View>
                </View>
            </View>
        );
    }
}
