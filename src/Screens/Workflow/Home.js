import React, {Component} from 'react';
import {
  AppRegistry,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Image,
  ScrollView,
  ImageBackground,
    Alert
} from 'react-native';

import {fetchFromLocal} from "../../Utils/SaveDetails";
import NativeAlertManager from "react-native/Libraries/Alert/NativeAlertManager";
import {getHistory} from "../Navigation/Networking";
import {APP_DISPLAY_NAME, INTERNET_CONNECTION_ISSUE} from "../../Constants/StringConstants";
import {showInfoAlert} from "../../Utils/AlertPopUp";

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      userName: '',
      userRank: 'Official',
        profile_pic: '',
        historyCount:0
    };
  }

  async componentDidMount() {
      let userInfo= await fetchFromLocal('loggedInUserDetails');
      userInfo = await JSON.parse(userInfo);
      console.log(JSON.stringify(userInfo));
      this.setState({isLoading:true})
      let historyResult = await getHistory(userInfo.access_token, userInfo.id);
      console.log("$$$$$$$$$$$$$$$$$$$$$$$=======================>" + JSON.stringify(historyResult));
      if (historyResult.success)
      {
          this.setState({isLoading:false})
          let countObject = historyResult.data[0];
          console.log("@@@@@@@@@@@@@@@@@@@>>>" + JSON.stringify(countObject));
          if (countObject) {
              let countValue = countObject["total_survey_count"]
              this.setState({historyCount: countValue})
          }
      }
      else {
          this.setState({isLoading:false})
          if (response.message === INTERNET_CONNECTION_ISSUE){
              Alert.alert(APP_DISPLAY_NAME,'Connect to the network.')
          } else {
              showInfoAlert(response.message)
          }
      }


      console.log('===>Home Screen: Component Did Mount');
      let userDetails = await fetchFromLocal('loggedInUserDetails');
      userDetails = await JSON.parse(userDetails);
      this.setState({
          userName: userDetails.first_name + ' ' + userDetails.last_name,
          profile_pic: userDetails.profile_pic !== null ? userDetails.profile_pic : '',
      });
  }


  async fetchHistory() {

  }

  startSurvey() {
      this.props.navigation.navigate('TournamentType');
    //this.props.navigation.navigate('TournamentList');
  }

  render() {
    const upperViewHeight = deviceHeight / 2 - 60;
    const imageSize = deviceWidth / 3 + 50;
    return (
      <View style={{flex: 1}}>
        <View
          style={{
            flex: 1,
            width: deviceWidth,
            height: upperViewHeight,
            backgroundColor: '#0E0F3F',
              justifyContent:'center',
              alignItems:'center'
          }}>
          <View
            style={{
              height: imageSize,
              width: imageSize,
                alignItems:'center',
              // marginTop: (upperViewHeight - imageSize) / 2 - 80,
              // marginLeft: (deviceWidth - imageSize) / 2,
            }}>
              {/*<TouchableOpacity style={{position:'absolute', width:50, height:40, marginTop:50, marginLeft:deviceWidth - 160}}>*/}
              {/*    <Image style={{width: 30, height: 30}} source={require('../../Images/Icons/Settings.png')}></Image>*/}
              {/*</TouchableOpacity>*/}
            {/*<TouchableOpacity style={{marginLeft: 10, height: 50, width: 50, borderWidth:1, borderColor:'white'}} />*/}
            <Image
                source={require('../../Images/Icons/Settings.png')}

              // source={{uri: this.state.profile_pic}}
              style={{
                width: imageSize - 65,
                height: imageSize - 65,
                borderRadius: 50,
                  alignItems:'center',
                  justifyContent:'center'
              }}
            />
            <View style={{alignItems: 'center'}}>
              <Text style={{color: 'white', fontSize: 20, fontWeight: '600'}}>
                {this.state.userName}
              </Text>
            </View>
            <View style={{alignItems: 'center', marginTop: 6}}>
              <Text style={{color: '#9495AA', fontSize: 14, fontWeight: '300'}}>
                {this.state.userRank}
              </Text>
            </View>
          </View>
        </View>
        <View
          style={{
            width: deviceWidth,
            height: deviceHeight / 2 + 60,
            backgroundColor: '#F6F7FB',
          }}
        />
        <View
          style={{
            width: deviceWidth - 60,
            height: 200,
            position: 'absolute',
            flexDirection: 'column',
            backgroundColor: 'white',
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: upperViewHeight - 50,
            marginLeft: 30,
            borderRadius: 8,
          }}>
          <Text style={{marginTop: 0, fontSize: 10}}>GET STARTED</Text>
          <Text style={{fontSize: 38, fontWeight: '600', marginTop: 10}}>
            Ready, Set, Go
          </Text>
          <Text style={{fontSize: 14, fontWeight: '200', marginTop: 15}}>
            Fill out a game survey now
          </Text>
          <TouchableOpacity
            onPress={() => this.startSurvey()}
            style={{
              borderRadius: 22,
              height: 44,
              width: 180,
              backgroundColor: '#EF4322',
              marginTop: 20,
            }}>
            <View style={{alignItems: 'center', marginTop: 12}}>
              <Text
                style={{
                  color: 'white',
                  alignItems: 'center',
                  justifyContent: 'center',
                  fontWeight: '600',
                  fontSize: 16,
                }}>
                Start
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: deviceWidth - 60,
            height: 100,
            position: 'absolute',
            flexDirection: 'row',
            backgroundColor: 'white',
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: upperViewHeight + 160,
            marginLeft: 30,
            borderRadius: 8,
          }}>
          <Image
            source={require('../../Images/Icons/Clock.png')}
            style={{width: 60, height: 60, marginLeft: -50}}
          />
          <View
            style={{
              flexDirection: 'column',
              width: deviceWidth - 260,
              marginLeft: 20,
            }}>
            <Text style={{color: 'black'}}>History</Text>
            <Text style={{fontSize: 24, fontWeight: '600'}}>{this.state.historyCount}</Text>
            <Text style={{color: '#B2B5BC', fontSize: 12}}>Submissions</Text>
          </View>
          <View style={{marginRight: -50, marginLeft: 30}}>
            <Image
              source={require('../../Images/Icons/Graph.png')}
              style={{width: 60, height: 60}}
            />
          </View>
        </View>
      </View>
    );
  }
}
