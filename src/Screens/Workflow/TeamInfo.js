import React, {Component} from 'react';
import {
    AppRegistry,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    Image,
    FlatList,
    Alert, ActivityIndicator
} from 'react-native';

import {
    INTERNET_CONNECTION_ISSUE,
    APP_DISPLAY_NAME,
    SELECTED_TEAMS
} from '../../Constants/StringConstants'
import {showInfoAlert} from "../../Utils/AlertPopUp";
import SurveyTopBar from '../../Components/SurveyTopBar';
import {fetchFromLocal, saveToLocal} from "../../Utils/SaveDetails";
import {getTournamentTeamList} from "../Navigation/Networking";

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;

export default class TeamInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      teamData: [],
        isLoading:false,
        selectedData:[],
      teamASelected:'',
      teamBSelected:'',
      selectedNumber:0
    };
  }

  async componentDidMount(){
      this.setState({isLoading:true})
      let userDetails= await fetchFromLocal('loggedInUserDetails');
      userDetails = await JSON.parse(userDetails);
      console.log('===================>>>>>?????' + JSON.stringify(userDetails));
      let tournamentId = this.props.navigation.getParam('id');
      let tournamentDate = this.props.navigation.getParam('date');
      console.log('--------------------->>>> Tournament date is =>' + tournamentDate);
      let message = JSON.stringify(userDetails) + JSON.stringify(tournamentId) + JSON.stringify(tournamentDate)
      let response = await getTournamentTeamList(userDetails.access_token, tournamentId, tournamentDate);
      if (response.success)
      {
          this.setState({isLoading:false})
          this.setState({teamData: response.data})
          await this.addSelectionFlag()
      }
      else {
          this.setState({isLoading:false})
          if (response.message === INTERNET_CONNECTION_ISSUE){
              Alert.alert(APP_DISPLAY_NAME,'Connect to the network.')
          } else {
              showInfoAlert(response.message)
          }
      }
  }

  addSelectionFlag() {
        var teamCollection = [];
        for(let i = 0; i < this.state.teamData.length; i++){
            var teamInfo = this.state.teamData[i];
            teamInfo.selected = false;
            teamCollection.push(teamInfo);
        }
        this.setState({teamData:teamCollection});
  }

  startSurvey(index) {

      let teamCollection = this.state.teamData;
      let selectedTeamNumber = this.state.selectedNumber;
      let teamInfo = teamCollection[index];

      if(this.state.selectedNumber < 2 && !teamInfo.selected){
          teamInfo.selected = !teamInfo.selected;
          teamCollection[index] = teamInfo;
          selectedTeamNumber = selectedTeamNumber + 1;

          var selectedTeam = this.state.selectedData;
          selectedTeam.push(teamInfo);
          this.setState({selectedData:selectedTeam})

          this.setState({selectedNumber:selectedTeamNumber});
          let that = this;
          if (selectedTeamNumber === 2) {
              setTimeout(async function () {
                  await saveToLocal(SELECTED_TEAMS, JSON.stringify(that.state.selectedData))
                  that.props.navigation.navigate('TeamExperience');
              }, 0);
          }
      }
      else {
          if (teamInfo.selected) {
              var selectedTeam = this.state.selectedData;
              var selectedIndex = selectedTeam.findIndex(p => p.team_name === teamInfo.team_name)
              selectedTeam.splice(selectedIndex, 1);
              this.setState({selectedData:selectedTeam})

              teamInfo.selected = !teamInfo.selected;
              selectedTeamNumber = selectedTeamNumber - 1;
              this.setState({selectedNumber:selectedTeamNumber});
          }
          teamCollection[index] = teamInfo;
      }
      this.setState({teamData:teamCollection});
  }

  render() {
    const upperViewHeight = deviceHeight/3; /// 2 - 160;
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
          {this.state.isLoading && <View style={{position:'absolute', top:0, bottom:0, zIndex:222, left:0, width:'100%', height:deviceHeight, flex:1, justifyContent:'center', alignItems:'center', backgroundColor:'black', opacity:0.5}}>
              <ActivityIndicator size="large" color="white" /></View>
          }
        <View
          style={{
            width: deviceWidth,
            height: upperViewHeight,
            backgroundColor: '#0E0F3F',
          }}>
          <SurveyTopBar step={3} navigation={this.props.navigation} />
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              marginLeft: 10,
              height: 100,
              width: deviceWidth,
            }}>
            <Text
              style={{
                color: 'white',
                width: deviceWidth - 40,
                fontSize: 38,
                fontWeight: 'bold',
              }}>
              What teams were playing?
            </Text>
          </View>
        </View>
          <View style={{width:deviceWidth, height:10, backgroundColor:'#E93D1B'}}>
          </View>
        <View
          style={{
            width: deviceWidth,
            height: deviceHeight*(2/3), // 2 + 160,
            backgroundColor: '#F6F7FB',
          }}>
          <FlatList
            data={this.state.teamData}
            renderItem={({item, index}) => (
              <View
                style={{
                  alignItems: 'flex-start',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  flex: 1,
                  height: 60,
                  margin: 5,
                  marginLeft: 30,
                  marginRight: 30,
                  backgroundColor: 'white',
                  borderRadius: 30,
                }}>
                <Text style={{marginTop: 20, marginLeft: 30, fontSize:16}}>{item.team_name}</Text>
                <Image
                  style={{height: 40, width: 40, marginTop:10, marginRight:14}}
                  source={item.selected ? require('../../Images/Icons/Selected.png') : require('../../Images/Icons/Add.png')}
                />
                <TouchableOpacity style={{position:'absolute', width: deviceWidth-60, height:60}} onPress={()=>this.startSurvey(index)}>
                </TouchableOpacity>
              </View>
            )}
            numColumns={1}
          />
        </View>
      </View>
    );
  }
}
