import React, {Component} from 'react';
import {
    AppRegistry,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    TextInput,
    Alert,
    TouchableHighlight
} from 'react-native';
import SurveyTopBar from "../../Components/SurveyTopBar";
import index from "react-native-fetch-blob";
import {saveToLocal} from "../../Utils/SaveDetails";
import {AGE_GROUP, GAME_TYPE, SELECTED_TEAMS} from "../../Constants/StringConstants";
import QuickPicker from "quick-picker";

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;

export default class SelectAge extends Component {
    constructor(props) {
        super(props);
        this.state = {
            signUpSelected: true,
            loginSelected: false,
            selectedNumber: 0,
            ageGroup:[
                "8U",
                "10U",
                "12U",
                "15U",
                "19U",
                "Mite",
                "Squirt",
                "PeeWee",
                "Bantam",
                "16U",
                "Jr gold"
            ],
            selectedAge:''
        };
    }

    selectAgeGroup() {
        //Dropdown()
        console.log("Sending post message");
        if (Platform.OS === 'ios'){
            let that = this;
            QuickPicker.open({
                items: this.state.ageGroup,
                selectedValue: this.state.selectedAge,
                onPressDone: this.onDoneclicked.bind(this),
                onValueChange: selectedValueFromPicker =>{
                    this.setState({ selectedAge: selectedValueFromPicker })}
            });
            this.setState({showPicker:true})
        }
    }

    onDoneclicked() {
        QuickPicker.close()
    }

    async moveToCompetitionLevel() {
        if (this.state.selectedAge !== '') {
            QuickPicker.close()
            await saveToLocal(AGE_GROUP,this.state.selectedAge)
            this.props.navigation.navigate('CompetitionLevel');
        }
    }


    render() {
        const upperViewHeight = deviceHeight / 3 - 40;
        return (
            <View>
                <View
                    style={{
                        width: deviceWidth,
                        height: upperViewHeight,
                        backgroundColor: '#0E0F3F',
                    }}>
                    <SurveyTopBar step={3} navigation={this.props.navigation}/>
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            marginLeft: 10,
                            height: 100,
                            width: deviceWidth,
                        }}>
                        <Text
                            style={{
                                color: 'white',
                                width: deviceWidth - 40,
                                fontSize: 38,
                                fontWeight: 'bold',
                            }}>
                            Select the age.
                        </Text>
                    </View>
                </View>
                <View
                    style={{
                        width: deviceWidth,
                        height: deviceHeight * (2/3) + 20,//+ 150,
                        backgroundColor: '#F6F7FB',
                    }}>
                    <View>
                        <Text style={{marginTop:10, marginLeft:15, fontSize:16}}> Age Group</Text>
                        <View>
                        <TouchableHighlight  style={{padding:20, height:40, backgroundColor:'#DDDDDD', marginLeft:20, marginRight:20, marginTop:10, borderRadius:10}}
                                             onPress={()=>this.selectAgeGroup()}>
                            <Text style={{color: 'black', height:40, fontSize:15, marginTop:-10}}>{this.state.selectedAge}</Text>
                        </TouchableHighlight>

                        </View>
                    </View>
                    <TouchableOpacity
                        style={{height:50,width:160, backgroundColor:'#E93D1B', marginBottom:30, alignItems:'center', justifyContent:'center', marginLeft:deviceWidth/2 - 80, borderRadius:25, marginTop:10}}
                        onPress={()=>this.moveToCompetitionLevel()}>
                        <Text style={{color:'white',fontSize:18, fontWeight:'600'}}>Next</Text>
                    </TouchableOpacity>
                    <QuickPicker/>
                </View>
            </View>
        );
    }
}
