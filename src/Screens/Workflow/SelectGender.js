import React, {Component} from 'react';
import {
    AppRegistry,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    Image,
    ScrollView,
    ImageBackground, FlatList,
} from 'react-native';
import SurveyTopBar from "../../Components/SurveyTopBar";
import index from "react-native-fetch-blob";
import {saveToLocal} from "../../Utils/SaveDetails";
import {GAME_TYPE, GENDER_TYPE, SELECTED_TEAMS} from "../../Constants/StringConstants";

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;

export default class SelectGender extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedNumber: 0,
            tournamentGender:[
                {name: 'Male'},
                {name: 'Female'}
            ],
            selectedData:[]
        };
    }

    async selectGender(index) {
        let teamCollection = this.state.tournamentGender;
        let selectedTeamNumber = this.state.selectedNumber;
        let teamInfo = teamCollection[index];

        if(this.state.selectedNumber < 1 && !teamInfo.selected){
            teamInfo.selected = !teamInfo.selected;
            teamCollection[index] = teamInfo;
            selectedTeamNumber = selectedTeamNumber + 1;

            var selectedGame = this.state.selectedData;
            selectedGame.push(teamInfo);
            this.setState({selectedData:selectedGame})

            this.setState({selectedNumber:selectedTeamNumber});
            let that = this;
            if (selectedTeamNumber === 1) {
                setTimeout(async function () {
                    await saveToLocal(GENDER_TYPE,JSON.stringify(that.state.selectedData))
                    that.props.navigation.navigate('SelectAge');
                }, 0);
            }
        }
        else {
            if (teamInfo.selected) {
                var selectedTeam = this.state.selectedData;
                var selectedIndex = selectedTeam.findIndex(p => p.team_name === teamInfo.team_name)
                selectedTeam.splice(selectedIndex, 1);
                this.setState({selectedData:selectedTeam})

                teamInfo.selected = !teamInfo.selected;
                selectedTeamNumber = selectedTeamNumber - 1;
                this.setState({selectedNumber:selectedTeamNumber});
            }
            teamCollection[index] = teamInfo;
        }
        this.setState({teamData:teamCollection});
    }

    moveToAge() {
        this.props.navigation.navigate('SelectAge');
    }

    render() {
        const upperViewHeight = deviceHeight / 3 - 40;
        return (
            <View>
                <View
                    style={{
                        width: deviceWidth,
                        height: upperViewHeight,
                        backgroundColor: '#0E0F3F',
                    }}>
                    <SurveyTopBar step={3} navigation={this.props.navigation}/>
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            marginLeft: 10,
                            height: 100,
                            width: deviceWidth,
                        }}>
                        <Text
                            style={{
                                color: 'white',
                                width: deviceWidth - 40,
                                fontSize: 38,
                                fontWeight: 'bold',
                            }}>
                            Select the gender.
                        </Text>
                    </View>
                </View>
                <View
                    style={{
                        width: deviceWidth,
                        height: deviceHeight * (2/3) + 20,//+ 150,
                        backgroundColor: '#F6F7FB',
                    }}>
                    <FlatList
                        data={this.state.tournamentGender}
                        renderItem={({item, index}) => (
                            <View
                                style={{
                                    alignItems: 'flex-start',
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    flex: 1,
                                    height: 60,
                                    margin: 5,
                                    marginLeft: 30,
                                    marginRight: 30,
                                    backgroundColor: 'white',
                                    borderRadius: 30,
                                }}>
                                <Text style={{marginTop: 20, marginLeft: 30, fontSize:16}}>{item.name}</Text>
                                <Image
                                    style={{height: 40, width: 40, marginTop:10, marginRight:14}}
                                    source={item.selected ? require('../../Images/Icons/Selected.png') : require('../../Images/Icons/Add.png')}
                                />
                                <TouchableOpacity style={{position:'absolute', width: deviceWidth-60, height:60}} onPress={()=>this.selectGender(index)}>
                                </TouchableOpacity>
                            </View>
                        )}
                    />
                    <TouchableOpacity
                        style={{height:50,width:160, backgroundColor:'#E93D1B', marginBottom:30, alignItems:'center', justifyContent:'center', marginLeft:deviceWidth/2 - 80, borderRadius:25}}
                        onPress={()=>this.moveToAge()}>
                        <Text style={{color:'white',fontSize:18, fontWeight:'600'}}>Next</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
