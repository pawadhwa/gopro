import React, {Component} from 'react';
import {
    AppRegistry,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    TextInput, Alert, TouchableHighlight
} from 'react-native';
import SurveyTopBar from "../../Components/SurveyTopBar";
import QuickPicker from "quick-picker";
import {saveToLocal} from "../../Utils/SaveDetails";
import {COMPETITION_LEVEL} from "../../Constants/StringConstants";

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;

export default class CompetitionLevel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            signUpSelected: true,
            loginSelected: false,
            selectedNumber: 0,
            competitionLevel:[
                "AA",
                "A",
                "B1",
                "B2",
                "B",
                "C",
            ],
            selectedLevel:''
        };
    }

    selectCompetitionLevel() {
        //Dropdown()
        console.log("Sending post message");
        if (Platform.OS === 'ios'){
            QuickPicker.open({
                items: this.state.competitionLevel,
                selectedValue: this.state.selectedLevel,
                onPressDone: this.onDoneClicked.bind(this),
                onValueChange: selectedValueFromPicker =>{
                    this.setState({ selectedLevel: selectedValueFromPicker })}
            });
            this.setState({showPicker:true})
        }
    }

    onDoneClicked() {
        QuickPicker.close()
    }

    async moveToTeamEntry() {
        if (this.state.selectedLevel !== '') {
            QuickPicker.close()
            await saveToLocal(COMPETITION_LEVEL, this.state.selectedLevel)
            this.props.navigation.navigate('ManualTeamEntry');
        }
    }

    render() {
        const upperViewHeight = deviceHeight / 3 - 40;
        return (
            <View>
                <View
                    style={{
                        width: deviceWidth,
                        height: upperViewHeight,
                        backgroundColor: '#0E0F3F',
                    }}>
                    <SurveyTopBar step={3} navigation={this.props.navigation}/>
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            marginLeft: 10,
                            height: 100,
                            width: deviceWidth,
                        }}>
                        <Text
                            style={{
                                color: 'white',
                                width: deviceWidth - 40,
                                fontSize: 38,
                                fontWeight: 'bold',
                            }}>
                            Select the competition level
                        </Text>
                    </View>
                </View>
                <View
                    style={{
                        width: deviceWidth,
                        height: deviceHeight * (2/3) + 20,//+ 150,
                        backgroundColor: '#F6F7FB',
                    }}>
                    <View>
                        <Text style={{marginTop:10, marginLeft:15, fontSize:16}}>Competition Level</Text>
                        <View>
                            <TouchableHighlight  style={{padding:20, height:40, backgroundColor:'#DDDDDD', marginLeft:20, marginRight:20, marginTop:10, borderRadius:10}}
                                                 onPress={()=>this.selectCompetitionLevel()}>
                                <Text style={{color: 'black', height:40, fontSize:15, marginTop:-10}}>{this.state.selectedLevel}</Text>
                            </TouchableHighlight>

                        </View>
                    </View>
                    <TouchableOpacity
                        style={{height:50,width:160, backgroundColor:'#E93D1B', marginBottom:30, alignItems:'center', justifyContent:'center', marginLeft:deviceWidth/2 - 80, borderRadius:25, marginTop:15}}
                        onPress={()=>this.moveToTeamEntry()}>
                        <Text style={{color:'white',fontSize:18, fontWeight:'600'}}>Next</Text>
                    </TouchableOpacity>
                    <QuickPicker/>
                </View>
            </View>
        );
    }
}
