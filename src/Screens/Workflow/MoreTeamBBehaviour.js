import React, {Component} from 'react';
import {
    AppRegistry,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    Image,
    FlatList,
} from 'react-native';
import SurveyTopBar from '../../Components/SurveyTopBar';
import {ScrollView} from "react-navigation";
import SurveySmiles from "../../Components/SurveySmiles";
import {fetchFromLocal, saveToLocal} from "../../Utils/SaveDetails";
import {SELECTED_TEAMS, TEAMA_SURVEY_DONE, TEAMB_SURVEY_DONE,
    HEAD_COACH_BEHAVIOUR,
    ASTT_COACH_BEHAVIOUR,
    FANS_BEHAVIOUR,
    PLAYER_BEHAVIOUR,
    TEAMA_COACH_IMPROVEMENT_REQUIRED,
    TEAMA_ASTTCOACH_IMPROVEMENT_REQUIRED,
    TEAMA_FANS_IMPROVEMENT_REQUIRED,
    TEAMA_PLAYERS_IMPROVEMENT_REQUIRED} from "../../Constants/StringConstants";

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;

export default class MoreTeamABehaviour extends Component {

    constructor(props){
        super(props);
        this.state = {
            teamName:'',
            isActive:1,
            headerQuestion:''
        }
    }

    async componentDidMount(){
        let selectedTeamData = await fetchFromLocal(SELECTED_TEAMS)
        let teamsData = await JSON.parse(selectedTeamData);

        this.setState({teamName: teamsData[0].team_name})
        let question = 'Tell us more about ' + this.state.teamName + ' behaviour.'
        this.setState({headerQuestion:question});
    }

    async moveToNext(nextScreen,toMoveNextScreen,question){

        let teamname = this.state.teamName;
        let question1 = teamname + " head coach behaviour";
        let question2 = teamname + " assistant coach behaviour";
        let question3 = teamname + " fans behaviour";
        let question4 = teamname + " players behaviour";

        let firstQuestionAnswered = await fetchFromLocal(question1)
        console.log('firstQuestionAnswered===>' + firstQuestionAnswered);
        let secQuestionAnswered = await fetchFromLocal(question2)
        console.log('secQuestionAnswered===>' + secQuestionAnswered);
        let thirdQuestionAnswered = await fetchFromLocal(question3)
        console.log('thirdQuestionAnswered===>' + thirdQuestionAnswered);
        let fourthQuestionAnswered = await fetchFromLocal(question4)
        console.log('fourthQuestionAnswered===>' + fourthQuestionAnswered);

        if ((firstQuestionAnswered === 'good') || (firstQuestionAnswered === 'great')) {
            await saveToLocal('TeamA coach improvement required','false')
            console.log('Local data saved');
        } else {
            await saveToLocal('TeamA coach improvement required','true')
            console.log('Local data saved');
        }

        if ((secQuestionAnswered === 'good') || (secQuestionAnswered === 'great')) {
            await saveToLocal('TeamA asttcoach improvement required','false')
            console.log('Local data saved');
        } else {
            await saveToLocal('TeamA asttcoach improvement required','true')
            console.log('Local data saved');
        }

        if ((thirdQuestionAnswered === 'good') || (thirdQuestionAnswered === 'great')) {
            await saveToLocal('TeamA fans improvement required','false')
            console.log('Local data saved');
        } else {
            await saveToLocal('TeamA fans improvement required','true')
            console.log('Local data saved');
        }

        if ((fourthQuestionAnswered === 'good') || (fourthQuestionAnswered === 'great')) {
            await saveToLocal('TeamA players improvement required','false')
            console.log('Local data saved');
        } else {
            await saveToLocal('TeamA players improvement required','true')
            console.log('Local data saved');
        }



        if ((firstQuestionAnswered !== 'good') || (firstQuestionAnswered !== 'great') || (firstQuestionAnswered !== 'terrible') || (firstQuestionAnswered !== 'bad') || (firstQuestionAnswered !== 'okay')){
            console.log('firstQuestionAnswered===>' + firstQuestionAnswered);
            this.moveToScreen(nextScreen,question,toMoveNextScreen)
            return;
        }
        else if ((secQuestionAnswered !== 'good') || (secQuestionAnswered !== 'great') || (secQuestionAnswered !== 'terrible') || (secQuestionAnswered !== 'bad') || (secQuestionAnswered !== 'okay')){
            console.log('secQuestionAnswered===>' + secQuestionAnswered);
            this.moveToScreen(nextScreen,question,toMoveNextScreen)
            return;
        }
        else if ((thirdQuestionAnswered !== 'good') || (thirdQuestionAnswered !== 'great') || (thirdQuestionAnswered !== 'terrible') || (thirdQuestionAnswered !== 'bad') || (thirdQuestionAnswered !== 'okay')){
            console.log('thirdQuestionAnswered===>' + thirdQuestionAnswered);
            this.moveToScreen(nextScreen,question,toMoveNextScreen)
            return;
        }
        else if ((fourthQuestionAnswered !== 'good') || (fourthQuestionAnswered !== 'great') || (fourthQuestionAnswered !== 'terrible') || (fourthQuestionAnswered !== 'bad') || (fourthQuestionAnswered !== 'okay')){
            console.log('fourthQuestionAnswered===>' + fourthQuestionAnswered);
            this.moveToScreen(nextScreen,question,toMoveNextScreen)
            return;
        }
    }


    async moveToScreen(nextScreen, question, toMoveNextScreen){
        if (nextScreen !== ''){

            let teamAQuestion = this.state.teamName + " players behaviour";

            if (question === teamAQuestion){
                await saveToLocal(TEAMA_SURVEY_DONE,JSON.stringify(true))
            }

            //Fetch the saved flag for filled survey for both teams
            let teamAImprovement = await this.getImprovementReport(true)

            if (!teamAImprovement){
                await saveToLocal('TeamAImprovement','false')
                this.props.navigation.navigate({
                    key:toMoveNextScreen,
                    routeName:toMoveNextScreen
                });
            } else {
                await saveToLocal('TeamAImprovement','true')
                this.props.navigation.navigate({
                    key:nextScreen,
                    routeName:nextScreen
                });
            }
        }
    }

    async getAllQuestions(ifTeamA){
        let teamname = this.state.teamName;
        let question1 = teamname + " head coach behaviour";
        let question2 = teamname + " assistant coach behaviour";
        let question3 = teamname + " fans behaviour";
        let question4 = teamname + " players behaviour";
        return [{question1},{question2},{question3},{question4}];
    }

    async getImprovementReport(ifTeamA) {
        let questArr = await this.getAllQuestions(ifTeamA)
        let isFeedbackReqiured = false;
        if (questArr.length) {
            for(let i = 0; i < questArr.length; i++){
                let questionDict = questArr[i];
                let question = '';
                if (i === 0){
                    question = questionDict.question1;
                }
                else if (i === 1) {
                    question = questionDict.question2;
                }
                else if (i === 2) {
                    question = questionDict.question3;
                }
                else if (i === 3) {
                    question = questionDict.question4;
                }
                let questAnswer = await fetchFromLocal(question);
                if ((questAnswer !== 'good') && (questAnswer !== 'great')){
                    isFeedbackReqiured = true;
                }
                console.log('============>' + question + '==========>' + questAnswer + '===========>' + isFeedbackReqiured)
            }
        }
        return isFeedbackReqiured;
    }

    active(v){
        this.setState({
            isActive : v
        })
    }

    toNavigateNext(screenName){
        let question = this.state.teamName + PLAYER_BEHAVIOUR;
        this.moveToNext('ImprovementScreenA','MoreTeamBBehaviour',question)
        this.props.navigation.navigate({
            key:screenName,
            routeName:screenName
        })
    }

    render() {

        let question1 = this.state.teamName + HEAD_COACH_BEHAVIOUR;
        let question2 = this.state.teamName + ASTT_COACH_BEHAVIOUR;
        let question3 = this.state.teamName + FANS_BEHAVIOUR;
        let question4 = this.state.teamName + PLAYER_BEHAVIOUR;


        const upperViewHeight = deviceHeight / 3; //2 - 130;
        return (
            <View style={{flex: 1}}>
                <View
                    style={{
                        flex: 1,
                        width: deviceWidth,
                        height: upperViewHeight,
                        backgroundColor: '#0E0F3F',
                    }}>
                    <SurveyTopBar step={4} navigation={this.props.navigation} fromScreen="MoreTeamABehaviour" questionOne={question1} questionSecond={question2} questionThird={question3} questionFourth={question4}/>
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            marginLeft: 10,
                            height: 100,
                            width: deviceWidth,
                        }}>
                        <Text
                            style={{
                                color: 'white',
                                width: deviceWidth - 40,
                                fontSize: 33,
                                fontWeight: 'bold',
                            }}>
                            {this.state.headerQuestion}
                        </Text>
                    </View>
                </View>
                <View style={{width:deviceWidth, height:10, backgroundColor:'#E93D1B'}}>
                </View>
                <View
                    style={{
                        width: deviceWidth,
                        height: deviceHeight * (2/3), //2 + 130,
                        backgroundColor: '#F6F7FB',
                    }}>
                    <ScrollView>
                        <SurveySmiles
                            answerText=''
                            activeCurrect={this.active.bind(this)}
                            currectActive={1}
                            isActive={this.state.isActive}
                            question={question1}
                            allQuestions=""
                            navigation={this.props.navigation}
                            toMoveScreen=""
                            finalScreen=""
                            toMoveNextScreen=""
                            currentScreen="MoreTeamABehaviour"/>
                        <SurveySmiles
                            answerText=''
                            activeCurrect={this.active.bind(this)}
                            currectActive={2}
                            isActive={this.state.isActive}
                            question={question2}
                            allQuestions=""
                            navigation={this.props.navigation}
                            toMoveScreen=""
                            finalScreen=""
                            toMoveNextScreen=""
                            currentScreen="MoreTeamABehaviour"/>
                        <SurveySmiles
                            answerText=''
                            activeCurrect={this.active.bind(this)}
                            currectActive={3}
                            isActive={this.state.isActive}
                            question={question3}
                            allQuestions=""
                            navigation={this.props.navigation}
                            toMoveScreen=""
                            finalScreen=""
                            toMoveNextScreen=""
                            currentScreen="MoreTeamABehaviour"/>
                        <SurveySmiles
                            answerText=''
                            activeCurrect={this.active.bind(this)}
                            currectActive={4}
                            isActive={this.state.isActive}
                            question={question4}
                            allQuestions={[{question1},{question2},{question3},{question4}]}
                            navigation={this.props.navigation}
                            toMoveScreen="ImprovementScreenA"
                            finalScreen=""
                            toMoveNextScreen="MoreTeamBBehaviour"
                            currentScreen="MoreTeamABehaviour"
                            navigateFromParent={this.toNavigateNext.bind(this)}/>
                    </ScrollView>
                    <TouchableOpacity
                        style={{height:50,width:160, backgroundColor:'#E93D1B', marginBottom:30, alignItems:'center', justifyContent:'center', marginLeft:deviceWidth/2 - 80, borderRadius:25}}
                        onPress={()=>this.moveToNext('ImprovementScreenA','MoreTeamBBehaviour',question4)}>
                        <Text style={{color:'white',fontSize:18, fontWeight:'600'}}>Next</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

}
