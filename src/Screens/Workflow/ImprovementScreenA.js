import React, {Component} from 'react';
import {
    AppRegistry,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    Image,
    FlatList
} from 'react-native';
import SurveyTopBar from '../../Components/SurveyTopBar';
import SurveySmiles from "../../Components/SurveySmiles";
import {fetchFromLocal, removeFromLocal, saveToLocal} from "../../Utils/SaveDetails";
import {
    SELECTED_TEAMS,
} from "../../Constants/StringConstants";
import index from "react-native-fetch-blob";

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;

export default class ImprovementScreenA extends Component {

    constructor(props) {
        super(props);
        this.state = {
            teamImprovementData: [
                {name: 'Respect the Players'},
                {name: 'Respect the Rules'},
                {name: 'Respect the Opponent'},
                {name: 'Respect the Official'},
            ],
            selectedNumber:0,
            selectedOptionsArray:[],
            teamName:'',
            headerQuestion:'',
            toShowHeadCoach:false,
            toShowAsttCoach:false,
            toShowPlayer:false,
            toShowFans:false,
            isTeamA: false
        };
    }


    async componentDidMount() {
        console.log('=================????????????????????' + JSON.stringify(this.props.navigation));

        let selectedTeamData = await fetchFromLocal(SELECTED_TEAMS)
        let teamsData = await JSON.parse(selectedTeamData);

        let selectedTeamInfo;
        console.log('=================????????????????????' + JSON.stringify(teamsData[0]));

        let navigationKey = this.props.navigation.state.key;
        if (navigationKey === 'ImprovementScreenA') {
            selectedTeamInfo = teamsData[0]
            this.setState({isTeamA:true})
            console.log('==============================================================>',selectedTeamInfo.teams_name);
            this.setState({teamName: selectedTeamInfo.team_name})
        } else if (navigationKey === 'ImprovementScreenB'){
            selectedTeamInfo = teamsData[1]
            this.setState({isTeamA:false})
            console.log('==============================================================>',selectedTeamInfo.team_name);
            this.setState({teamName: selectedTeamInfo.team_name})
        }

        let headerMessage = 'Where can ' + this.state.teamName +' improve?'
        this.setState({headerQuestion:headerMessage});
    }



    async moveToNext(index){

        let teamString = this.state.isTeamA ? 'TEAMA' : 'TEAMB';

        let gameMessageString = teamString + '_RESPECT_GAME_REQUIRED';
        let playersMessageString = teamString + '_RESPECT_PLAYERS_REQUIRED';
        let rulesMessageString = teamString + '_RESPECT_RULES_REQUIRED';
        let opponentMessageString = teamString + '_RESPECT_OPPONENT_REQUIRED';
        let officialMessageString = teamString + '_RESPECT_OFFICIAL_REQUIRED';


        await saveToLocal(gameMessageString,'false')
        console.log('============================>');
        await saveToLocal(playersMessageString,'false')
        console.log('============================>');
        await saveToLocal(rulesMessageString,'false')
        console.log('============================>');
        await saveToLocal(opponentMessageString,'false')
        console.log('============================>');
        await saveToLocal(officialMessageString,'false')
        console.log('============================>');


        console.log('===============> Btn Action called')
        if (this.state.selectedNumber === 0)
            return;


        let selectedArray = this.state.selectedOptionsArray;

        for(let i = 0; i < selectedArray.length; i++) {
            //console.log(JSON.stringify(selectedOptions));
            let teamInfo = selectedArray[i];
            console.log(JSON.stringify(teamInfo));
            if (teamInfo.name === 'Respect the Game') {
                await saveToLocal(gameMessageString,'true')
                console.log('######################')
            } else if (teamInfo.name === 'Respect the Players') {
                await saveToLocal(playersMessageString,'true')
                console.log('######################')
            } else if (teamInfo.name === 'Respect the Rules') {
                await saveToLocal(rulesMessageString,'true')
                console.log('######################')
            } else if (teamInfo.name === 'Respect the Opponent') {
                await saveToLocal(opponentMessageString,'true')
                console.log('######################')
            } else if (teamInfo.name === 'Respect the Official') {
                await saveToLocal(officialMessageString,'true')
                console.log('######################')
            }
        }

        let teamAGameRecord = await fetchFromLocal(gameMessageString);
        let teamAGameFeedbackRequired = JSON.parse(teamAGameRecord);
        console.log('######################')
        let teamAPlayersRequired = await fetchFromLocal(playersMessageString);
        let teamAPlayersFeedbackRequired = JSON.parse(teamAPlayersRequired);
        console.log('######################')
        let teamARulesRequired = await fetchFromLocal(rulesMessageString);
        let teamARulesFeedbackRequired = JSON.parse(teamARulesRequired);
        console.log('######################')
        let teamAOpponentRequired = await fetchFromLocal(opponentMessageString);
        let teamAOpponentFeedbackRequired = JSON.parse(teamAOpponentRequired);
        console.log('######################')
        let teamAOfficialRequired = await fetchFromLocal(officialMessageString);
        let teamAOfficialFeedbackRequired = JSON.parse(teamAOfficialRequired);
        console.log('######################')


        if (teamAPlayersFeedbackRequired) {
            let navigationString = this.props.navigation.state.key === 'ImprovementScreenA' ? 'ImprovementSolPlayers' : 'ImprovementSolPlayersA';
            this.props.navigation.navigate({
                key:navigationString,
                routeName: navigationString
            })
        } else if (teamARulesFeedbackRequired) {
            let navigationString = this.props.navigation.state.key === 'ImprovementScreenA' ? 'ImprovementSolRules' : 'ImprovementSolRulesA';
            this.props.navigation.navigate({
                key:navigationString,
                routeName: navigationString
            })
        } else if (teamAOpponentFeedbackRequired) {
            let navigationString = this.props.navigation.state.key === 'ImprovementScreenA' ? 'ImprovementSolOpponent' : 'ImprovementSolOpponentA';
            this.props.navigation.navigate({
                key:navigationString,
                routeName: navigationString
            })
        } else if (teamAOfficialFeedbackRequired) {
            let navigationString = this.props.navigation.state.key === 'ImprovementScreenA' ? 'ImprovementSolOfficials' : 'ImprovementSolOfficialsA';
            this.props.navigation.navigate({
                key:navigationString,
                routeName: navigationString
            })
        }
    }


    async submitImprovementReport(index){

        let teamCollection = this.state.teamImprovementData;
        let selectedTeamNumber = this.state.selectedNumber;
        let teamInfo = teamCollection[index];
        if (teamInfo.selected) {
            let teamInfo = teamCollection[index];
            if (teamInfo.selected) {

                let sampleArray = this.state.selectedOptionsArray;
                if (teamInfo.selected){
                    let deleteionIndex = sampleArray.indexOf(teamInfo);
                    sampleArray.splice(deleteionIndex, 1);
                }
                this.setState({selectedOptionsArray:[]})

                console.log('3........' + JSON.stringify(sampleArray));
                this.setState({selectedOptionsArray:sampleArray})
                console.log('3........' + JSON.stringify(this.state.selectedOptionsArray));


                teamInfo.selected = !teamInfo.selected;
                selectedTeamNumber = selectedTeamNumber - 1;


                if (this.props.navigation.state.key === 'ImprovementScreenA') {
                    await saveToLocal('Team A improvement options',JSON.stringify(this.state.selectedOptionsArray))
                } else if (this.props.navigation.state.key === 'ImprovementScreenB') {
                    await saveToLocal('Team B improvement options',JSON.stringify(this.state.selectedOptionsArray))
                }
                this.setState({selectedNumber:selectedTeamNumber});
            }
            teamCollection[index] = teamInfo;
        } else {
            let teamInfo = teamCollection[index];
            teamInfo.selected = !teamInfo.selected;
            teamCollection[index] = teamInfo;
            selectedTeamNumber = selectedTeamNumber + 1;
            this.setState({selectedNumber:selectedTeamNumber});
            let saveKey = '';

            if (teamInfo.selected){
                let testArray = this.state.selectedOptionsArray;
                let testArra2 = [...testArray, teamInfo];
                this.setState({selectedOptionsArray:[]},()=>{
                    this.setState({selectedOptionsArray:testArra2});
                });
            }

            console.log('3........' + JSON.stringify(this.state.selectedOptionsArray));

            if (this.props.navigation.state.key === 'ImprovementScreenA') {
                await saveToLocal('Team A improvement options',JSON.stringify(this.state.selectedOptionsArray))
            } else if (this.props.navigation.state.key === 'ImprovementScreenB') {
                await saveToLocal('Team B improvement options',JSON.stringify(this.state.selectedOptionsArray))
            }
        }

        this.setState({teamImprovementData:teamCollection});
    }

    render() {
        const upperViewHeight = (deviceHeight / 3) - 20;//2 - 150;
        return (
            <View style={{flex:1, backgroundColor:'white'}}>
                <View
                    style={{
                        width: deviceWidth,
                        height: upperViewHeight,
                        backgroundColor: '#0E0F3F',
                    }}>
                    <SurveyTopBar step={4} navigation={this.props.navigation} fromScreen="ImprovementScreenA"/>
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            marginLeft: 10,
                            height: 100,
                            width: deviceWidth,
                        }}>
                        <Text
                            style={{
                                color: 'white',
                                width: deviceWidth - 40,
                                fontSize: 33,
                                fontWeight: 'bold',
                            }}>
                            {this.state.headerQuestion}
                        </Text>
                    </View>
                    <View style={{width:deviceWidth, height:10, backgroundColor:'#E93D1B'}}/>
                </View>
                <View
                    style={{
                        width: deviceWidth,
                        height: deviceHeight * (2/3) + 20,//+ 150,
                        backgroundColor: '#F6F7FB',
                    }}>
                    <FlatList
                        data={this.state.teamImprovementData}
                        renderItem={({item, index}) => (
                            <View
                                style={{
                                    alignItems: 'flex-start',
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    flex: 1,
                                    height: 60,
                                    margin: 5,
                                    marginLeft: 30,
                                    marginRight: 30,
                                    backgroundColor: 'white',
                                    borderRadius: 30,
                                }}>
                                <Text style={{marginTop: 20, marginLeft: 30, fontSize:16}}>{item.name}</Text>
                                <Image
                                    style={{height: 40, width: 40, marginTop:10, marginRight:14}}
                                    source={item.selected ? require('../../Images/Icons/Selected.png') : require('../../Images/Icons/Add.png')}
                                />
                                <TouchableOpacity style={{position:'absolute', width: deviceWidth-60, height:60}} onPress={()=>this.submitImprovementReport(index)}>
                                </TouchableOpacity>
                            </View>
                        )}
                    />
                    <TouchableOpacity
                        style={{height:50,width:160, backgroundColor:'#E93D1B', marginBottom:30, alignItems:'center', justifyContent:'center', marginLeft:deviceWidth/2 - 80, borderRadius:25}}
                        onPress={()=>this.moveToNext(index)}>
                        <Text style={{color:'white',fontSize:18, fontWeight:'600'}}>Next</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
