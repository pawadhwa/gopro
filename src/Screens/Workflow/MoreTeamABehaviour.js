import React, {Component} from 'react';
import {
    AppRegistry,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    Image,
    FlatList,
} from 'react-native';
import SurveyTopBar from '../../Components/SurveyTopBar';
import {ScrollView} from "react-navigation";
import SurveySmiles from "../../Components/SurveySmiles";
import {fetchFromLocal, saveToLocal} from "../../Utils/SaveDetails";
import {
    SELECTED_TEAMS, TEAMA_SURVEY_DONE, TEAMB_SURVEY_DONE,
    HEAD_COACH_BEHAVIOUR,
    ASTT_COACH_BEHAVIOUR,
    FANS_BEHAVIOUR,
    PLAYER_BEHAVIOUR, TAKE_REPORT_DATA
} from "../../Constants/StringConstants";

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;

export default class MoreTeamABehaviour extends Component {

    constructor(props){
        super(props);
        this.state = {
            teamName:'',
            isActive:1,
            headerQuestion:'',
            isTeamA:false,
            isBBehaviourRequired:false
        }
    }

    async componentDidMount(){
        let selectedTeamData = await fetchFromLocal(SELECTED_TEAMS)
        let teamsData = await JSON.parse(selectedTeamData);

        let navigationKey = this.props.navigation.state.key;
        if (navigationKey === 'MoreTeamBBehaviour') {
            this.setState({teamName: teamsData[1].team_name})
            this.setState({isTeamA:false})
        } else {
            this.setState({teamName: teamsData[0].team_name})
            this.setState({isTeamA:true})
        }
        let question = 'Tell us more about ' + this.state.teamName + ' behaviour.'
        this.setState({headerQuestion:question});

        //Fetch team B feedback status to be asked or not
        let questionB = teamsData[1].team_name + " behaviour"
        let teamBAnswer = await fetchFromLocal(questionB);

        if ((teamBAnswer === 'terrible') || (teamBAnswer === 'bad')) {
            this.setState({isBBehaviourRequired:true})
        } else {
            this.setState({isBBehaviourRequired:false})
        }

    }

    async moveToNext(nextScreen,toMoveNextScreen){

        let teamname = this.state.teamName;
        let isTeamFeedbackRequired = false;
        let question1 = teamname + " head coach behaviour";
        let question2 = teamname + " assistant coach behaviour";
        let question3 = teamname + " fans behaviour";
        let question4 = teamname + " players behaviour";

        let firstQuestionAnswered = await fetchFromLocal(question1)
        console.log('firstQuestionAnswered===>' + firstQuestionAnswered);
        let secQuestionAnswered = await fetchFromLocal(question2)
        console.log('secQuestionAnswered===>' + secQuestionAnswered);
        let thirdQuestionAnswered = await fetchFromLocal(question3)
        console.log('thirdQuestionAnswered===>' + thirdQuestionAnswered);
        let fourthQuestionAnswered = await fetchFromLocal(question4)
        console.log('fourthQuestionAnswered===>' + fourthQuestionAnswered);


        if (firstQuestionAnswered && secQuestionAnswered && thirdQuestionAnswered && fourthQuestionAnswered)
        {
            if (((firstQuestionAnswered === 'good') || (firstQuestionAnswered === 'great') || (firstQuestionAnswered === 'okay')) && ((secQuestionAnswered === 'good') || (secQuestionAnswered === 'great') || (secQuestionAnswered === 'okay')) && ((thirdQuestionAnswered === 'good') || (thirdQuestionAnswered === 'great') || (thirdQuestionAnswered === 'okay')) && ((fourthQuestionAnswered === 'good') || (fourthQuestionAnswered === 'great') || (fourthQuestionAnswered === 'okay'))) {
                // await saveToLocal('TeamA coach improvement required','false')
                console.log('Local data saved');
            }
            else {
                //await saveToLocal('TeamA players improvement required','true')
                isTeamFeedbackRequired = true;
                console.log('Local data saved');
            }


            let toSaveMessage = this.state.teamName + 'feedback required'
            await saveToLocal(toSaveMessage,JSON.stringify(isTeamFeedbackRequired));

            console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@=====>' + nextScreen + '&&&&&&&&&' + toMoveNextScreen);

            if (isTeamFeedbackRequired){
                this.props.navigation.navigate({
                    key:this.state.isTeamA ? 'ImprovementScreenA' : 'ImprovementScreenB',
                    routeName:isTeamFeedbackRequired ? nextScreen : toMoveNextScreen
                });
            } else {
                let navigationKey = this.props.navigation.state.key;
                if (navigationKey === 'MoreTeamBBehaviour') {
                    let feedBackRequired = await fetchFromLocal(TAKE_REPORT_DATA);
                    if (feedBackRequired) {
                        let feedBackCount = JSON.parse(feedBackRequired)
                        if (feedBackCount > 1) {
                            this.props.navigation.navigate({
                                key: 'FeedbackScreen',
                                routeName: 'FeedbackScreen'
                            })
                        }  else {
                            this.props.navigation.navigate({
                                key: 'FinalSubmission',
                                routeName: 'FinalSubmission'
                            })
                        }
                    } else {
                        this.props.navigation.navigate({
                            key: 'FinalSubmission',
                            routeName: 'FinalSubmission'
                        })
                    }
                } else {
                    if (this.state.isBBehaviourRequired) {
                        this.props.navigation.navigate({
                            key:'MoreTeamBBehaviour',
                            routeName:'MoreTeamBBehaviour'
                        });
                    } else {
                        let feedBackRequired = await fetchFromLocal(TAKE_REPORT_DATA);
                        if (feedBackRequired) {
                            let feedBackCount = JSON.parse(feedBackRequired)
                            if (feedBackCount > 1) {
                                this.props.navigation.navigate({
                                    key: 'FeedbackScreen',
                                    routeName: 'FeedbackScreen'
                                })
                            }  else {
                                this.props.navigation.navigate({
                                    key: 'FinalSubmission',
                                    routeName: 'FinalSubmission'
                                })
                            }
                        } else {
                            this.props.navigation.navigate({
                                key: 'FinalSubmission',
                                routeName: 'FinalSubmission'
                            })
                        }
                    }
                }
            }
        }
    }


    async getAllQuestions(ifTeamA){
        let teamname = this.state.teamName;
        let question1 = teamname + " head coach behaviour";
        let question2 = teamname + " assistant coach behaviour";
        let question3 = teamname + " fans behaviour";
        let question4 = teamname + " players behaviour";
        return [{question1},{question2},{question3},{question4}];
    }

    async getImprovementReport(ifTeamA) {
        let questArr = await this.getAllQuestions(ifTeamA)
        let isFeedbackReqiured = false;
        if (questArr.length) {
            for(let i = 0; i < questArr.length; i++){
                let questionDict = questArr[i];
                let question = '';
                if (i === 0){
                    question = questionDict.question1;
                }
                else if (i === 1) {
                    question = questionDict.question2;
                }
                else if (i === 2) {
                    question = questionDict.question3;
                }
                else if (i === 3) {
                    question = questionDict.question4;
                }
                let questAnswer = await fetchFromLocal(question);
                if ((questAnswer !== 'good') && (questAnswer !== 'great')){
                    isFeedbackReqiured = true;
                }
                console.log('============>' + question + '==========>' + questAnswer + '===========>' + isFeedbackReqiured)
            }
        }
        return isFeedbackReqiured;
    }

    active(v){
        this.setState({
            isActive : v
        })

        if (this.state.isActive === 4) {
            this.scrollView.scrollToEnd({ animated: true });
        }

    }

    toNavigateNext(screenName){

        this.moveToNext('ImprovementScreenA','MoreTeamBBehaviour')
    }

    render() {

        let question1 = this.state.teamName + HEAD_COACH_BEHAVIOUR;
        let question2 = this.state.teamName + ASTT_COACH_BEHAVIOUR;
        let question3 = this.state.teamName + FANS_BEHAVIOUR;
        let question4 = this.state.teamName + PLAYER_BEHAVIOUR;


        const upperViewHeight = deviceHeight / 3; //2 - 130;
        return (
            <View style={{flex: 1}}>
                <View
                    style={{
                        flex: 1,
                        width: deviceWidth,
                        height: upperViewHeight,
                        backgroundColor: '#0E0F3F',
                    }}>
                    <SurveyTopBar step={4} navigation={this.props.navigation} fromScreen="MoreTeamABehaviour" questionOne={question1} questionSecond={question2} questionThird={question3} questionFourth={question4}/>
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            marginLeft: 10,
                            height: 100,
                            width: deviceWidth,
                        }}>
                        <Text
                            style={{
                                color: 'white',
                                width: deviceWidth - 40,
                                fontSize: 33,
                                fontWeight: 'bold',
                            }}>
                            {this.state.headerQuestion}
                        </Text>
                    </View>
                </View>
                <View style={{width:deviceWidth, height:10, backgroundColor:'#E93D1B'}}>
                </View>
                <View
                    style={{
                        width: deviceWidth,
                        height: deviceHeight * (2/3), //2 + 130,
                        backgroundColor: '#F6F7FB',
                    }}>
                    <ScrollView
                        ref={(view) => {
                            this.scrollView = view;
                        }}
                    >
                    <SurveySmiles
                        answerText=''
                        activeCurrect={this.active.bind(this)}
                        currectActive={1}
                        isActive={this.state.isActive}
                        question={question1}
                        allQuestions=""
                        navigation={this.props.navigation}
                        toMoveScreen=""
                        finalScreen=""
                        toMoveNextScreen=""
                        currentScreen="MoreTeamABehaviour"/>
                    <SurveySmiles
                        answerText=''
                        activeCurrect={this.active.bind(this)}
                        currectActive={2}
                        isActive={this.state.isActive}
                        question={question2}
                        allQuestions=""
                        navigation={this.props.navigation}
                        toMoveScreen=""
                        finalScreen=""
                        toMoveNextScreen=""
                        currentScreen="MoreTeamABehaviour"/>
                    <SurveySmiles
                        answerText=''
                        activeCurrect={this.active.bind(this)}
                        currectActive={3}
                        isActive={this.state.isActive}
                        question={question3}
                        allQuestions=""
                        navigation={this.props.navigation}
                        toMoveScreen=""
                        finalScreen=""
                        toMoveNextScreen=""
                        currentScreen="MoreTeamABehaviour"/>
                    <SurveySmiles
                        answerText=''
                        activeCurrect={this.active.bind(this)}
                        currectActive={4}
                        isActive={this.state.isActive}
                        question={question4}
                        allQuestions={[{question1},{question2},{question3},{question4}]}
                        navigation={this.props.navigation}
                        toMoveScreen="ImprovementScreenA"
                        finalScreen=""
                        toMoveNextScreen="MoreTeamBBehaviour"
                        currentScreen="MoreTeamABehaviour"
                        navigateFromParent={this.toNavigateNext.bind(this)}/>
                    </ScrollView>
                    <TouchableOpacity
                        style={{height:50,width:160, backgroundColor:'#E93D1B', marginBottom:30, alignItems:'center', justifyContent:'center', marginLeft:deviceWidth/2 - 80, borderRadius:25}}
                        onPress={()=>this.moveToNext('ImprovementScreenA','MoreTeamBBehaviour')}>
                        <Text style={{color:'white',fontSize:18, fontWeight:'600'}}>Next</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

}
