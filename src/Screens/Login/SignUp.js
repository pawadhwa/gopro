import React, {Component} from 'react';
import {
  AppRegistry,
  View,
  Text,
  TextInput,
  ImageBackground,
  Dimensions,
  Platform,
  TouchableOpacity, ScrollView, ActivityIndicator, Alert, StyleSheet,
} from 'react-native';
import LoginTopBar from '../../Components/LoginTopBar';

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;
const paddingValue = Platform.isPad ? true : false;
import { EMAIL_VALIDATION_MESSAGE,
    SIGNUP_ERROR,
    PASSWORD_VALIDATION_MESSAGE,
    CNF_PASSWORD_VALIDATION_MESSAGE,
    NAME_VALIDATION_MESSAGE,
    NAME_VALIDATION_DIGIT,
    EMAIL_EMPTY_MESSAGE,
    PASSWORD_EMPTY_MESSAGE,
    CONFIRM_PASSWORD_EMPTY_MESSAGE,
    LOGIN_ERROR,
    BLANK_EMAIL_ID_MESSAGE,
    INTERNET_CONNECTION_ISSUE,
    APP_DISPLAY_NAME,
    PHONE_NUMBER_EMPTY,
    PHONE_NUMBER_DIGIT_ONLY} from '../../Constants/StringConstants'
import {validEmail, validPassword, validName, checkDigit, validPhoneNumer} from "../../Utils/Validations";
import {performSignup} from "../../Screens/Navigation/Networking";
import {showInfoAlert} from "../../Utils/AlertPopUp";

export default class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      phoneNumber: '',
      email: '',
      hockeyNumber: '',
      password: '',
      isLoading:false
    };
  }

    async userSignUp(){
        console.log('userSignUp() called');
        let firstName = this.state.firstName;
        let lastname = this.state.lastName;
        let phoneNumber = this.state.phoneNumber;
        let userEmail = this.state.email;
        let hockeyNumber = this.state.hockeyNumber;
        let userPassword = this.state.password;

        console.log('validating');


        if (validName(firstName)){
            showInfoAlert(NAME_VALIDATION_MESSAGE)
            return;
        }

        if (validName(lastname)){
            showInfoAlert(NAME_VALIDATION_MESSAGE)
            return;
        }

        if (validPhoneNumer(phoneNumber)){
            showInfoAlert(PHONE_NUMBER_EMPTY)
            return;
        }

        if (!checkDigit(phoneNumber)) {
            showInfoAlert(PHONE_NUMBER_DIGIT_ONLY)
            return
        }


        if (validPhoneNumer(hockeyNumber)){
            showInfoAlert(PHONE_NUMBER_EMPTY)
            return;
        }


        if (this.state.email.length <= 0){
            showInfoAlert(BLANK_EMAIL_ID_MESSAGE)
            return;
        }

        if (validEmail(userEmail)){
            showInfoAlert(EMAIL_VALIDATION_MESSAGE)
            return;
        }

        if (this.state.password.length < 1) {
            showInfoAlert(PASSWORD_EMPTY_MESSAGE)
            return;
        }

        if (validPassword(userPassword)){
            showInfoAlert(PASSWORD_VALIDATION_MESSAGE)
            return;
        }


        let platfrom = Platform.OS === 'ios' ? 'ios' : 'android';

        let sendData ={
            first_name : firstName,
            last_name: lastname,
            email : userEmail,
            password: userPassword,
            mobile_number: phoneNumber,
            official_number: hockeyNumber,
            device_registered_from: platfrom,
            device_token: 'sdfsddff'
        }


        this.setState({isLoading:true})
        let response = await performSignup(sendData);
        console.log("=== Signup response => "+JSON.stringify(response));
        if (response.success){
            this.setState({isLoading:false})

            Alert.alert(
                APP_DISPLAY_NAME,
                response.message,
                [
                    {
                        text: 'Ok', onPress: () => {
                            this.props.navigation.goBack();
                        }
                    },
                ],
                {cancelable: false},
            )

        } else {
            this.setState({isLoading:false})
            if (response.message === INTERNET_CONNECTION_ISSUE){
                Alert.alert(APP_DISPLAY_NAME,'Connect to the network to Sign up.')
            } else {
                showInfoAlert(response.message)
            }
        }
    };


    onTextChange(text) {
        var cleaned = ('' + text).replace(/\D/g, '')
        var match = cleaned.match(/(\d{3})(\d{3})(\d{4})$/)
        if (match) {
            var intlCode = (match[1] ? '+1 ' : ''),
                number = ['(', match[2], ') ', match[3], '-', match[4]].join('');

            this.setState({ phoneNumber: number });
            Alert.alert('Inside with number' + number)
            return;
        }

        // Alert.alert('Outside with number' + text)
        this.setState({ phoneNumber: text });
    }


  render() {
    return (
      <View style={{flex:1}}>
        <ImageBackground
          source={require('../../Images/HomeBack.png')}
          style={{
            flex: 1,
            width: '100%',
            resizeMode: 'stretch',
            justifyContent: 'center',
            height: deviceHeight,
          }}>
            <LoginTopBar navigation={this.props.navigation} />
            {this.state.isLoading && <View style={{position:'absolute', top:0, bottom:0, zIndex:222, left:0, width:'100%', height:deviceHeight, flex:1, justifyContent:'center', alignItems:'center', backgroundColor:'black', opacity:0.5}}>
                <ActivityIndicator size="large" color="white" /></View>
            }
            <ScrollView style={{flex: 1}}>
          <View style={{height:deviceHeight+150}}>
              <View
                style={{
                  width: deviceWidth,
                  height: 16,
                  marginTop: 80,
                }}>
                <Text
                  style={{
                    marginLeft: paddingValue ? 140 : 40,
                    color: '#808FAE',
                    fontSize: 13,
                  }}>
                  FIRST NAME
                </Text>
              </View>
              <TextInput
                onChangeText={text => this.setState({firstName: text})}
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                style={{
                  textAlign: 'left',
                  width: deviceWidth - (paddingValue ? 280 : 80),
                  marginLeft: paddingValue ? 140 : 40,
                  marginBottom: 7,
                  height: Platform.OS === 'ios' ? 40 : 45,
                  borderRadius: 5,
                  fontSize: 18,
                  color: 'white',
                  paddingHorizontal: 5,
                  borderBottomWidth: 1,
                  borderBottomColor: '#808FAE',
                }}
              />
            {/*<View style={{marginTop:10}}>*/}
              <View
                style={{
                  width: deviceWidth,
                  height: 16,
                  marginTop: 10,
                }}>
                <Text
                  style={{
                    marginLeft: paddingValue ? 140 : 40,
                    color: '#808FAE',
                    fontSize: 13,
                  }}>
                  LAST NAME
                </Text>
              </View>
              <TextInput
                onChangeText={text => this.setState({lastName: text})}
                underlineColorAndroid="transparent"
                secureTextEntry={false}
                style={{
                  textAlign: 'left',
                  width: deviceWidth - (paddingValue ? 280 : 80),
                  marginLeft: paddingValue ? 140 : 40,
                  marginBottom: 7,
                  height: Platform.OS === 'ios' ? 40 : 45,
                  borderRadius: 5,
                  fontSize: 18,
                  color: 'white',
                  borderBottomWidth: 1,
                  borderBottomColor: '#808FAE',
                  paddingHorizontal: 5,
                }}
              />
            {/*</View>*/}
            {/*<View style={{flex: 1, width: deviceWidth, marginTop: 10}}>*/}
              <View
                style={{
                  width: deviceWidth,
                  height: 16,
                  marginTop: 10,
                }}>
                <Text
                  style={{
                    marginLeft: paddingValue ? 140 : 40,
                    color: '#808FAE',
                    fontSize: 13,
                  }}>
                  PHONE NUMBER
                </Text>
              </View>
              <TextInput
                onChangeText={text => this.setState({phoneNumber: text})}
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                keyboardType={"numeric"}
                style={{
                  textAlign: 'left',
                  width: deviceWidth - (paddingValue ? 280 : 80),
                  marginLeft: paddingValue ? 140 : 40,
                  marginBottom: 7,
                  height: Platform.OS === 'ios' ? 40 : 45,
                  borderRadius: 5,
                  fontSize: 18,
                  color: 'white',
                  paddingHorizontal: 5,
                  borderBottomWidth: 1,
                  borderBottomColor: '#808FAE',
                }}
              />
            {/*</View>*/}
            {/*<View style={{flex: 1, width: deviceWidth, marginTop: 10}}>*/}
              <View
                style={{
                  width: deviceWidth,
                  height: 16,
                  marginTop: 10,
                }}>
                <Text
                  style={{
                    marginLeft: paddingValue ? 140 : 40,
                    color: '#808FAE',
                    fontSize: 13,
                  }}>
                  EMAIL
                </Text>
              </View>
              <TextInput
                onChangeText={text => this.setState({email: text})}
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                style={{
                  textAlign: 'left',
                  width: deviceWidth - (paddingValue ? 280 : 80),
                  marginLeft: paddingValue ? 140 : 40,
                  marginBottom: 7,
                  height: Platform.OS === 'ios' ? 40 : 45,
                  borderRadius: 5,
                  fontSize: 18,
                  color: 'white',
                  paddingHorizontal: 5,
                  borderBottomWidth: 1,
                  borderBottomColor: '#808FAE',
                }}
              />
            {/*</View>*/}
            {/*<View style={{flex: 1, width: deviceWidth, marginTop: 10}}>*/}
              <View
                style={{
                  width: deviceWidth,
                  height: 16,
                  marginTop: 10,
                }}>
                <Text
                  style={{
                    marginLeft: paddingValue ? 140 : 40,
                    color: '#808FAE',
                    fontSize: 13,
                  }}>
                  USE HOCKEY OFFICIAL NUMBER
                </Text>
              </View>
              <TextInput
                onChangeText={text => this.setState({hockeyNumber: text})}
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                style={{
                  textAlign: 'left',
                  width: deviceWidth - (paddingValue ? 280 : 80),
                  marginLeft: paddingValue ? 140 : 40,
                  marginBottom: 7,
                  height: Platform.OS === 'ios' ? 40 : 45,
                  borderRadius: 5,
                  fontSize: 18,
                  color: 'white',
                  paddingHorizontal: 5,
                  borderBottomWidth: 1,
                  borderBottomColor: '#808FAE',
                }}
              />
            {/*</View>*/}
            {/*<View style={{flex: 1, width: deviceWidth, marginTop: 10}}>*/}
              <View
                style={{
                  width: deviceWidth,
                  height: 16,
                  marginTop: 10,
                }}>
                <Text
                  style={{
                    marginLeft: paddingValue ? 140 : 40,
                    color: '#808FAE',
                    fontSize: 13,
                  }}>
                  PASSWORD
                </Text>
              </View>
              <TextInput
                onChangeText={text => this.setState({password: text})}
                underlineColorAndroid="transparent"
                secureTextEntry={true}
                autoCapitalize="none"
                style={{
                  textAlign: 'left',
                  width: deviceWidth - (paddingValue ? 280 : 80),
                  marginLeft: paddingValue ? 140 : 40,
                  marginBottom: 7,
                  height: Platform.OS === 'ios' ? 40 : 45,
                  borderRadius: 5,
                  fontSize: 18,
                  color: 'white',
                  paddingHorizontal: 5,
                  borderBottomWidth: 1,
                  borderBottomColor: '#808FAE',
                }}
              />
            {/*</View>*/}
            <TouchableOpacity
              style={{
                backgroundColor: 'white',
                width: deviceWidth - 80,
                height: 48,
                marginLeft: 40,
                marginTop: 15,
                borderRadius: 24,
              }}
              onPress={() => this.userSignUp()}>
              <View
                style={{
                  alignItems: 'center',
                  marginTop: 12,
                  borderRadius: 22,
                }}>
                <Text
                  style={{
                    fontSize: 18,
                    color: '#293247',
                  }}>
                  Sign Up
                </Text>
              </View>
            </TouchableOpacity>
          </View>
            </ScrollView>
        </ImageBackground>
      </View>
        // </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    backgroundImage: {
        flex: 1,
        width: '100%',
        height: deviceHeight,
        justifyContent: 'flex-start'
    },
    logoImage: {
        width: 60,
        height: 60,
        marginTop: 20,
    },
    title: {
        fontSize: 25,
        color: '#569509'
    },
    subContainer: {
        flex: 1,
        color: '#FFFFFF',
    },
    contentContainer: {
        width: deviceWidth-40,
        backgroundColor: '#FFFFFF',
        height: deviceHeight-200,
        marginTop: 20,
        marginLeft: 20,
        borderRadius: 4,
        position: 'absolute',
        alignItems: 'center'
    },
    header: {
        fontSize: 20,
        color: '#568123',
    },
    signUpheader: {
        fontSize: 12
    },
    titleHeader: {
        fontSize: 20
    },
    signInButton: {
        height: 44,
        width: deviceWidth-80,
        backgroundColor: '#0B4C18',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4
    },
    socialButtons: {
        flexDirection: 'row',
        marginTop: 20,
        width: deviceWidth-120,
        height: 100,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 80
    },
    socioButtons: {
        height: 40,
        width: 40,
    },
    textSize:{
        fontSize: 12
    },
    signUpText:{
        fontSize: 12,
        color: '#005E20',
        fontWeight:'bold'
    },
    buttonContainer:{
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'space-between',
        width:100
    },
    spinnerView:{
        position:'absolute',
        top:0,
        bottom:0,
        zIndex:222,
        left:0,
        width:'100%',
        height:'100%',
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#ccd',
        opacity:.5
    }
});
