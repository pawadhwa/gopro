import React, {Component} from 'react';
import {
  AppRegistry,
  View,
  TextInput,
  ImageBackground,
  Dimensions,
  KeyboardAvoidingView,
  Text,
  Platform,
  TouchableOpacity,
  Alert,
  ActivityIndicator
} from 'react-native';

import { EMAIL_VALIDATION_MESSAGE,
    SIGNUP_ERROR,
    PASSWORD_VALIDATION_MESSAGE,
    CNF_PASSWORD_VALIDATION_MESSAGE,
    NAME_VALIDATION_MESSAGE,
    NAME_VALIDATION_DIGIT,
    EMAIL_EMPTY_MESSAGE,
    PASSWORD_EMPTY_MESSAGE,
    CONFIRM_PASSWORD_EMPTY_MESSAGE,
    LOGIN_ERROR,
    BLANK_EMAIL_ID_MESSAGE,
    INTERNET_CONNECTION_ISSUE,
    APP_DISPLAY_NAME,
    PHONE_NUMBER_EMPTY,
    PHONE_NUMBER_DIGIT_ONLY} from '../../Constants/StringConstants'

import {validEmail, validPassword, validConfirmPassword, validName, checkDigit, validPhoneNumer} from "../../Utils/Validations";
import LoginTopBar from '../../Components/LoginTopBar';
import {showInfoAlert} from "../../Utils/AlertPopUp";
import {saveToLocal} from "../../Utils/SaveDetails";
import {performLogin} from "../Navigation/Networking";
let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;
const paddingValue = Platform.isPad ? true : false;

export default class SignIn extends Component {

    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
            isLoading: false,
        };
    }

  async goForLogin() {
      console.log('perform login called');
      let userEmail = this.state.email;
      let userPassword = this.state.password;

      if (userEmail.length <= 0){
          showInfoAlert(EMAIL_EMPTY_MESSAGE)
          return;
      }

      if (validEmail(userEmail)){
          showInfoAlert(EMAIL_VALIDATION_MESSAGE)
          return;
      }

      if (userPassword.length <= 0){
          showInfoAlert(PASSWORD_EMPTY_MESSAGE)
          return;
      }
      else {
          this.setState({isLoading:true});
          let response = await performLogin(userEmail,userPassword);
          console.log("Response => "+JSON.stringify(response));
          if (response.success){
              this.setState({isLoading:false});
              await saveToLocal('loggedInUserDetails',JSON.stringify(response.data))
              //await this.getConfigurationOptions()
              this.props.navigation.navigate('Home');
          } else {
              this.setState({isLoading:false});
              if (response.message === INTERNET_CONNECTION_ISSUE){
                  Alert.alert(APP_DISPLAY_NAME,INTERNET_CONNECTION_ISSUE)
              } else {
                  showInfoAlert(response.message)
              }
          }
      }
    console.log('Login Performed');
  }

  render() {
    return (
      <View>
        <ImageBackground
          source={require('../../Images/HomeBack.png')}
          style={{
            flex: 1,
            width: '100%',
            resizeMode: 'stretch',
            justifyContent: 'center',
            height: deviceHeight,
          }}>
          <View style={{flex: 1}}>
              {this.state.isLoading && <View style={{position:'absolute', top:0, bottom:0, zIndex:222, left:0, width:'100%', height:deviceHeight, flex:1, justifyContent:'center', alignItems:'center', backgroundColor:'black', opacity:0.5}}>
                  <ActivityIndicator size="large" color="white" /></View>
              }
              <View style={{marginTop:10}}>
                <LoginTopBar navigation={this.props.navigation} />
              </View>
            <View style={{flex: 1, width: deviceWidth, marginTop: deviceHeight/4 - 50}}>
              <View
                style={{
                  width: deviceWidth,
                  height: 16,
                  marginTop: 80,
                }}>
                <Text
                  style={{
                    marginLeft: paddingValue ? 140 : 40,
                    color: '#808FAE',
                    fontSize: 13,
                  }}>
                  EMAIL
                </Text>
              </View>
              <TextInput
                onChangeText={text => this.setState({email: text})}
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                style={{
                  textAlign: 'left',
                  width: deviceWidth - (paddingValue ? 280 : 80),
                  marginLeft: paddingValue ? 140 : 40,
                  marginBottom: 7,
                  height: Platform.OS === 'ios' ? 40 : 45,
                  borderRadius: 5,
                  fontSize: 18,
                  color: 'white',
                  paddingHorizontal: 5,
                  borderBottomWidth: 1,
                  borderBottomColor: '#808FAE',
                }}
              />
            </View>
            <View style={{marginTop: 50}}>
              <View
                style={{
                  width: deviceWidth,
                  height: 16,
                  marginTop: 110,
                }}>
                <Text
                  style={{
                    marginLeft: paddingValue ? 140 : 40,
                    color: '#808FAE',
                    fontSize: 13,
                  }}>
                  PASSWORD
                </Text>
              </View>
              <TextInput
                onChangeText={text => this.setState({password: text})}
                underlineColorAndroid="transparent"
                secureTextEntry={true}
                style={{
                  textAlign: 'left',
                  width: deviceWidth - (paddingValue ? 280 : 80),
                  marginLeft: paddingValue ? 140 : 40,
                  marginBottom: 7,
                  height: Platform.OS === 'ios' ? 40 : 45,
                  borderRadius: 5,
                  fontSize: 18,
                  color: 'white',
                  borderBottomWidth: 1,
                  borderBottomColor: '#808FAE',
                  paddingHorizontal: 5,
                }}
              />
              <TouchableOpacity style={{width:150, height:35, backgroundColor:'clear', alignItems:'flex-end', marginLeft:deviceWidth-190}}>
                  <Text style={{color:'#808FAE', alignItems:'flex-start', marginTop:10}}>Forgot Password</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  backgroundColor: 'white',
                  width: deviceWidth - 80,
                  height: 48,
                  marginLeft: 40,
                  marginTop: 20,
                  borderRadius: 24,
                }}
                onPress={() => this.goForLogin()}>
                <View
                  style={{
                    alignItems: 'center',
                    marginTop: 10,
                    borderRadius: 22,
                  }}>
                  <Text
                    style={{
                      fontSize: 18,
                      color: '#293247',
                    }}>
                    Log in
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}
