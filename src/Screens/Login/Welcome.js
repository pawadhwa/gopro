import React, {Component} from 'react';
import {
  AppRegistry,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Image,
  ScrollView,
  ImageBackground,
} from 'react-native';

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;

export default class Welcome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      signUpSelected: true,
      loginSelected: false,
    };
  }

  goToSignIn() {
    this.setState({signUpSelected: false});
    this.setState({loginSelected: true});
    this.props.navigation.navigate('SignIn');
  }

  goToSignUp() {
    this.setState({signUpSelected: true});
    this.setState({loginSelected: false});
    this.props.navigation.navigate('SignUp');
  }

  render() {
    return (
      <ScrollView style={{flex: 1}}>
        <View style={{width: '100%', flex: 1}}>
          <ImageBackground
            source={require('../../Images/HomeBack.png')}
            style={{
              flex: 1,
              width: '100%',
              resizeMode: 'stretch',
              justifyContent: 'center',
              height: deviceHeight,
            }}>
            <View style={{marginTop: -100, alignItems: 'center'}}>
              <Image
                source={require('../../Images/Icons/ProLogo.png')}
                style={{width: 154, height: 200}}
                resizeMode='contain'
              />
              <View>
                <Text style={{color: 'white', fontSize: 24, marginTop: 35}}>
                  Create an account or log in:
                </Text>
              </View>
              <TouchableOpacity
                style={{
                  width: deviceWidth - 40,
                  height: 50,
                  marginTop: 30,
                  backgroundColor: this.state.signUpSelected ? 'white' : 'transparent',
                  borderWidth: 1,
                    borderColor: 'white',
                  borderRadius: 25,
                }}
                onPress={() => this.goToSignUp()}>
                <View style={{alignItems: 'center', marginTop: 13}}>
                  <Text
                    style={{
                      fontSize: 18,
                      color: this.state.signUpSelected ? '#293247' : 'white',
                    }}>
                    Sign up to create account
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  width: deviceWidth - 40,
                  height: 50,
                  marginTop: 30,
                  backgroundColor: this.state.loginSelected ? 'white' : 'transparent',
                  borderWidth: 1,
                    borderColor:'white',
                  borderRadius: 25,
                }}
                onPress={() => this.goToSignIn()}>
                <View style={{alignItems: 'center', marginTop: 13}}>
                  <Text
                    style={{
                      fontSize: 18,
                      color: this.state.loginSelected ? '#293247' : 'white',
                    }}>
                    Log in to an existing account
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </ImageBackground>
        </View>
      </ScrollView>
    );
  }
}
