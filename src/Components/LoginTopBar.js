import React, {Component} from 'react';
import {
  AppRegistry,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Image,
} from 'react-native';

let deviceWidth = Dimensions.get('window').width;

export default class LoginTopBar extends Component {
  goBack() {
    this.props.navigation.pop();
  }

  render() {
    return (
      <View style={{height: 80, width: deviceWidth, backgroundColor: 'clear'}}>
        <TouchableOpacity
          onPress={() => this.goBack()}
          style={{marginTop:34, marginLeft: 10, height: 40, width: 40}}>
          <Image
            style={{height: 24, width: 13}}
            source={require('../Images/Icons/Back.png')}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
