import React, {Component} from 'react';
import {
  AppRegistry,
  View,
  Dimensions,
  TouchableOpacity,
  Image,
} from 'react-native';
import {fetchFromLocal, removeFromLocal, saveToLocal} from "../Utils/SaveDetails";
import {TAKE_REPORT_DATA} from "../Constants/StringConstants";

let deviceWidth = Dimensions.get('window').width;

export default class SurveyTopBar extends Component {
  componentDidMount() {}

  async goBack() {
      if ((this.props.fromScreen === 'MoreTeamABehaviour') || (this.props.fromScreen === 'MoreTeamBBehaviour')){
          let resValue1 = await removeFromLocal(this.props.questionOne)
          let resValue2 = await removeFromLocal(this.props.questionSecond)
          let resValue3 = await removeFromLocal(this.props.questionThird)
          let resValue4 = await removeFromLocal(this.props.questionFourth)
      }
      else if (this.props.fromScreen === 'TeamExperience'){
          let resValue1 = await removeFromLocal(this.props.questionOne)
          let resValue2 = await removeFromLocal(this.props.questionSecond)
      }
      else if ((this.props.fromScreen === 'ImprovementScreenA') || (this.props.fromScreen === 'ImprovementScreenB')){
          //let resValue1 = await removeFromLocal(this.props.questionOne)
      }

      if (this.props.reportChecked) {
          let reportData = await fetchFromLocal(TAKE_REPORT_DATA);
          let reportCount = reportData ? JSON.parse(reportData) : 0;
          await saveToLocal(TAKE_REPORT_DATA,JSON.stringify(reportCount-1))
      }

      this.props.navigation.pop();
  }

  render() {
    let indicatorPadding = deviceWidth - 130;
    let unSelectedImage = require('../Images/Icons/ProgressUnselected.png');
    let currentImage = require('../Images/Icons/ProgressSelected.png');
    let selectedImage = require('../Images/Icons/ProgressActive.png');
    return (
      <View
        style={{
          height: 70,
          width: deviceWidth,
          backgroundColor: 'clear',
          flexDirection: 'row',
        }}>
        <TouchableOpacity
          onPress={() => this.goBack()}
          style={{marginTop: 44, marginLeft: 10, height: 40, width: 40, zIndex:222}}>
          <Image
            style={{height: 24, width: 13}}
            source={require('../Images/Icons/Back.png')}
          />
        </TouchableOpacity>
        <View
          style={{
            flexDirection: 'row',
            width: deviceWidth,
            height: 40,
            marginTop: 35,
            marginLeft: 20,
            position: 'absolute',
          }}>
          <Image
            source={this.props.step == 1 ? currentImage : selectedImage}
            style={{height: this.props.step == 1 ? 20 : 12, width: this.props.step == 1 ? 20 : 12, marginTop: this.props.step == 1 ? 10 : 15, marginLeft: 15}}
          />
          <View
            style={{
              height: 2,
              width: indicatorPadding / 4 - 10,
              marginTop: 20,
              marginLeft: 5,
              marginRight: 5,
              backgroundColor: this.props.step < 2 ? '#28284E' : 'red',
            }}
          />
          <Image
            source={
              this.props.step == 2
                ? currentImage
                : this.props.step > 2
                ? selectedImage
                : unSelectedImage
            }
            style={{height: this.props.step == 2 ? 20 : 12, width: this.props.step == 2 ? 20 : 12, marginTop: this.props.step == 2 ? 12 : 14}}
          />
          <View
            style={{
              height: 2,
              marginTop: 20,
              marginLeft: 5,
              marginRight: 5,
              backgroundColor: this.props.step < 3 ? '#28284E' : 'red',
              width: indicatorPadding / 4 - 10,
            }}
          />
          <Image
            source={
              this.props.step == 3
                ? currentImage
                : this.props.step > 3
                ? selectedImage
                : unSelectedImage
            }
            style={{height: this.props.step == 3 ? 20 : 12, width: this.props.step == 3 ? 20 : 12, marginTop: this.props.step == 3 ? 12 : 14}}
          />
          <View
            style={{
              height: 2,
              width: indicatorPadding / 4 - 10,
              marginTop: 20,
              marginLeft: 5,
              marginRight: 5,
              backgroundColor: this.props.step < 4 ? '#28284E' : 'red',
            }}
          />
          <Image
            source={
              this.props.step == 4
                ? currentImage
                : this.props.step > 4
                ? selectedImage
                : unSelectedImage
            }
            style={{height: this.props.step == 4 ? 20 : 12, width: this.props.step == 4 ? 20 : 12, marginTop: this.props.step == 4 ? 12 : 14}}
          />
            <View
                style={{
                    height: 2,
                    width: indicatorPadding / 4 - 10,
                    marginTop: 20,
                    marginLeft: 5,
                    marginRight: 5,
                    backgroundColor: this.props.step < 5 ? '#28284E' : 'red',
                }}
            />
            <Image
                source={
                    this.props.step == 5
                        ? currentImage
                        : this.props.step > 5
                        ? selectedImage
                        : unSelectedImage
                }
                style={{height: 16, width: 16, marginTop: 12}}
            />
        </View>
      </View>
    );
  }
}
