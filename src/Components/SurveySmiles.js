import React, {Component} from 'react';
import {
  AppRegistry,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Image,
  ScrollView,
  ImageBackground,
  Platform,
  FlatList,
  StyleSheet,
  Alert,
} from 'react-native';
import {fetchFromLocal, getTeamBeahviour, saveSurveyAnswers, saveToLocal} from "../Utils/SaveDetails";
import {SELECTED_TEAMS, TEAMA_SURVEY_DONE, TEAMB_SURVEY_DONE} from "../Constants/StringConstants";

let deviceWidth = Dimensions.get('window').width;
let deviceHeight = Dimensions.get('window').height;
const paddingValue = Platform.isPad ? true : false;

export default class SurveySmiles extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isActive: true,
        teamAName:'',
        teamBName:'',
        isAnswerGiven: false,
        isDelayAnswerGiven: false,
        isGoodAnswerProvided: false,
        isBadAnswerProvided: false,
        isOkayAnswerProvided: false,
        isTerribleAnswerProvided: false,
        isGreatAnswerProvided: false,
        isDelayGoodAnswerProvided: false,
        isDelayBadAnswerProvided: false,
        isDelayOkayAnswerProvided: false,
        isDelayTerribleAnswerProvided: false,
        isDelayGreatAnswerProvided: false,
        selectedOption:'',
        updateAnswer:false,
        questAnswer:''
    };
  }

  async componentDidMount() {

      let selectedTeamData = await fetchFromLocal(SELECTED_TEAMS)
      let teamsData = await JSON.parse(selectedTeamData);
      this.setState({teamAName: teamsData[0].team_name})
      this.setState({teamBName: teamsData[1].team_name})

      await getTeamBeahviour(this.props.question).then(r => {
          if (r) {
              this.setState({isAnswerGiven: true});
              this.setState({questAnswer:r});
          } else {
              this.setState({isAnswerGiven: false});
              this.setState({questAnswer:''});
          }
      });
  }

  async updateAnswer(answerValue, toUpdate, toMove) {

      this.setState({updateAnswer:toUpdate});
      if (toUpdate){
          this.setState({isAnswerGiven:false});
          this.updateStateValues(answerValue, toUpdate, toMove);
          this.props.activeCurrect(this.props.currectActive);
          this.setState({questAnswer:answerValue});
          return;
      }

      console.log('TO SAVE:' + this.props.question + 'WITH ANSWER:' + answerValue);
      await saveSurveyAnswers(this.props.question, answerValue).then(r => {
          console.log('=========>', JSON.stringify(r))
      });
      this.props.activeCurrect(this.props.isActive + 1);
      this.updateStateValues(answerValue, toUpdate, toMove);
      this.setState({questAnswer:answerValue});
  }


  moveToNextScreen(that, toMove){
      if (toMove){
          let currentScope = this;
          let nextScreen = this.props.toMoveScreen;
          setTimeout(async function () {
              if ((nextScreen !== '') && (currentScope.props.currentScreen === 'TeamExperience')) {
                  let selectedTeamData = await fetchFromLocal(SELECTED_TEAMS)
                  let teamsData = await JSON.parse(selectedTeamData);
                  let teamABehaviour = teamsData[0].team_name + " behaviour";
                  let teamBBehaviour = teamsData[1].team_name + " behaviour";

                  let teamAAnswer = await fetchFromLocal(teamABehaviour)
                  let teamBAnswer = await fetchFromLocal(teamBBehaviour)

                  if (((teamAAnswer === 'good') || (teamAAnswer === 'great') || (teamAAnswer === 'okay')) && ((teamBAnswer === 'good') || (teamBAnswer === 'great') || (teamBAnswer === 'okay'))) {
                      currentScope.props.navigation.navigate({
                          key: 'FinalSubmission',
                          routeName: 'FinalSubmission'
                      });
                  }
                  else if (((teamAAnswer === 'terrible') || (teamAAnswer === 'bad')) && ((teamBAnswer === 'terrible') || (teamBAnswer === 'bad'))) {
                      currentScope.props.navigation.navigate(nextScreen);
                  }
                  else if (((teamAAnswer === 'good') || (teamAAnswer === 'great') || (teamAAnswer === 'okay')) && ((teamBAnswer === 'terrible') || (teamBAnswer === 'bad'))) {
                      currentScope.props.navigation.navigate({
                          key: 'MoreTeamBBehaviour',
                          routeName: 'MoreTeamBBehaviour'
                      });
                  } else if (((teamAAnswer === 'terrible') || (teamAAnswer === 'bad')) && ((teamBAnswer === 'good') || (teamBAnswer === 'great') || (teamBAnswer === 'okay'))) {
                      currentScope.props.navigation.navigate(nextScreen);
                  }
              }
              else if ((nextScreen !== '') && (currentScope.props.currentScreen === 'MoreTeamABehaviour')) {

                  let teamAQuestion = currentScope.state.teamAName + " players behaviour";

                  if (currentScope.props.question === teamAQuestion){
                      await saveToLocal(TEAMA_SURVEY_DONE,JSON.stringify(true))
                  }

                  //Fetch the saved flag for filled survey for both teams
                  let teamAImprovement = await currentScope.getImprovementFeedback(true)

                  if (!teamAImprovement){
                      await saveToLocal('TeamAImprovement','false')
                      currentScope.props.navigateFromParent(currentScope.props.toMoveNextScreen);
                  } else {
                      await saveToLocal('TeamAImprovement','true')
                      currentScope.props.navigateFromParent(nextScreen);
                  }
              }

              else if ((nextScreen !== '') && (currentScope.props.currentScreen === 'MoreTeamBBehaviour')) {
                  let teamBQuestion = currentScope.state.teamBName + " players behaviour";
                  if (currentScope.props.question === teamBQuestion) {
                      await saveToLocal(TEAMB_SURVEY_DONE,JSON.stringify(true))
                  }
                  let teamBImprovement = await currentScope.getImprovementFeedback(false)
                  if (!teamBImprovement){
                      await saveToLocal('TeamBImprovement','false')
                      currentScope.props.navigateFromParent(currentScope.props.toMoveNextScreen, currentScope);
                  } else {
                      await saveToLocal('TeamBImprovement','true')
                      currentScope.props.navigateFromParent(nextScreen, currentScope);
                  }
              }
          }, 1000);
      }
  }

  getQuestions(ifTeamA){
      let teamname = ifTeamA ? this.state.teamAName :  this.state.teamBName;
      let question1 = teamname + " head coach behaviour";
      let question2 = teamname + " assistant coach behaviour";
      let question3 = teamname + " fans behaviour";
      let question4 = teamname + " players behaviour";
      return [{question1},{question2},{question3},{question4}];
  }


   async getImprovementFeedback(ifTeamA) {
      let questArr = await this.getQuestions(ifTeamA)
       let isRoughlyRated = false;
      if (questArr.length) {
          for(let i = 0; i < questArr.length; i++){
              let questionDict = questArr[i];
              let question = '';
              if (i === 0){
                  question = questionDict.question1;
              }
              else if (i === 1) {
                  question = questionDict.question2;
              }
              else if (i === 2) {
                  question = questionDict.question3;
              }
              else if (i === 3) {
                  question = questionDict.question4;
              }
              let questAnswer = await fetchFromLocal(question);
              if ((questAnswer !== 'good') && (questAnswer !== 'great')){
                  isRoughlyRated = true;
              }
          }
      }
      return isRoughlyRated;
    }

  updateStateValues(answerValue, toUpdate, toMove) {
      let that = this;
      switch (answerValue) {
          case 'good':
              this.setState({
                  isAnswerGiven: toUpdate ? false : true,
                  isGoodAnswerProvided: !this.state.isGoodAnswerProvided,
                  isBadAnswerProvided: false,
                  isOkayAnswerProvided: false,
                  isTerribleAnswerProvided: false,
                  isGreatAnswerProvided: false
              });

              that.setState({
                  isDelayAnswerGiven: toUpdate ? false : true,
                  isDelayGoodAnswerProvided: !that.state.isDelayGoodAnswerProvided,
                  isDelayBadAnswerProvided: false,
                  isDelayOkayAnswerProvided: false,
                  isDelayTerribleAnswerProvided: false,
                  isDelayGreatAnswerProvided: false,
              });
              this.moveToNextScreen(that, toMove)
              return;
          case 'terrible':
              this.setState({
                  isAnswerGiven: toUpdate ? false : true,
                  isTerribleAnswerProvided: !this.state.isTerribleAnswerProvided,
                  isGoodAnswerProvided: false,
                  isBadAnswerProvided: false,
                  isOkayAnswerProvided: false,
                  isGreatAnswerProvided: false
              });

              that.setState({
                  isDelayAnswerGiven: toUpdate ? false : true,
                  isDelayGoodAnswerProvided: false,
                  isDelayBadAnswerProvided: false,
                  isDelayOkayAnswerProvided: false,
                  isDelayTerribleAnswerProvided: !that.state.isDelayTerribleAnswerProvided,
                  isDelayGreatAnswerProvided: false,
              });

              this.moveToNextScreen(that, toMove)
              return;
          case 'bad':
              this.setState({
                  isAnswerGiven: toUpdate ? false : true,
                  isBadAnswerProvided: !this.state.isBadAnswerProvided,
                  isGoodAnswerProvided: false,
                  isTerribleAnswerProvided: false,
                  isOkayAnswerProvided: false,
                  isGreatAnswerProvided: false
              });

              that.setState({
                  isDelayAnswerGiven: toUpdate ? false : true,
                  isDelayGoodAnswerProvided: false,
                  isDelayBadAnswerProvided: !that.state.isDelayBadAnswerProvided,
                  isDelayOkayAnswerProvided: false,
                  isDelayTerribleAnswerProvided: false,
                  isDelayGreatAnswerProvided: false,
              });

              this.moveToNextScreen(toMove, toMove)
              return;
          case 'great':
              this.setState({
                  isAnswerGiven: toUpdate ? false : true,
                  isGreatAnswerProvided: !this.state.isGreatAnswerProvided,
                  isGoodAnswerProvided: false,
                  isTerribleAnswerProvided: false,
                  isOkayAnswerProvided: false,
                  isBadAnswerProvided: false
              });

              that.setState({
                  isDelayAnswerGiven: toUpdate ? false : true,
                  isDelayGoodAnswerProvided: false,
                  isDelayBadAnswerProvided: false,
                  isDelayOkayAnswerProvided: false,
                  isDelayTerribleAnswerProvided: false,
                  isDelayGreatAnswerProvided: !that.state.isDelayGreatAnswerProvided,
              });

              this.moveToNextScreen(that, toMove)
              return;
          case 'okay':
              this.setState({
                  isAnswerGiven: toUpdate ? false : true,
                  isOkayAnswerProvided: !this.state.isOkayAnswerProvided,
                  isGoodAnswerProvided: false,
                  isBadAnswerProvided: false,
                  isTerribleAnswerProvided: false,
                  isGreatAnswerProvided: false
              });

              that.setState({
                  isDelayAnswerGiven: toUpdate ? false : true,
                  isDelayGoodAnswerProvided: false,
                  isDelayBadAnswerProvided: false,
                  isDelayOkayAnswerProvided: !that.state.isDelayOkayAnswerProvided,
                  isDelayTerribleAnswerProvided: false,
                  isDelayGreatAnswerProvided: false,
              });

              this.moveToNextScreen(that, toMove)
              return;
      }
  }

  render() {
      let isEnable = this.props.currectActive == this.props.isActive;
      let imageName = require('../Images/Icons/Good.png');
      if (this.state.questAnswer === 'great'){
          imageName = require('../Images/Icons/Great.png');
      }
      else if (this.state.questAnswer === 'okay'){
          imageName = require('../Images/Icons/Okay.png');
      }
      else if (this.state.questAnswer === 'bad'){
          imageName = require('../Images/Icons/Bad.png');
      }
      else if (this.state.questAnswer === 'terrible'){
          imageName = require('../Images/Icons/Terrible.png');
      }
      return (
      <View
        style={{
          flexDirection: 'column',
          height: this.state.isDelayAnswerGiven ? 100: 150,
          alignItems: 'flex-start',
          justifyContent: 'space-between',
        }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            height: 50,
            alignItems: 'flex-start',
              marginTop:20,
            justifyContent: 'space-between',
            width: deviceWidth - 40,
            marginLeft: 20,
          }}>
          <View style={{flexDirection: 'row', alignItems:'flex-start', justifyContent:'space-between', width:this.state.isDelayAnswerGiven ? deviceWidth - 100 : deviceWidth - 40}}>
            <Text
              style={{
                  width: this.state.isDelayAnswerGiven ? deviceWidth - 160 : deviceWidth - 50,
                color: 'black',
                fontSize: 18,
                marginLeft: 20,
                  marginRight:10,
                marginTop: 10,
                  opacity: isEnable ? 1.0 : this.state.isAnswerGiven ? 1.0 : this.props.currectActive === 1 ? 1.0 : this.state.updateAnswer ? 1.0 : 0.2
              }}>
              {this.props.question}
            </Text>
              {this.state.isDelayAnswerGiven &&
              <View style={{flexDirection: 'row', marginRight:10}}>
                  <TouchableOpacity onPress={()=>this.updateAnswer(this.state.questAnswer, true, false)}>
                      <View style={{flexDirection: 'row'}}>
                  <Image
                      style={{height: 40, width: 40, marginRight: 4}}
                      source={imageName}
                  />
                  <Text style={{marginTop: 10}}>{this.state.questAnswer}</Text>
                      </View>
                  </TouchableOpacity>
              </View>}
          </View>
        </View>
          {!this.state.isDelayAnswerGiven &&
        <View style={{flex: 1, marginBottom:5, flexDirection:'row', justifyContent:'space-between', width: deviceWidth - 40}}>
          <View
            style={{
              flex: 1,
              marginTop: 10,
              backgroundColor: 'white',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'flex-start',
              width: deviceWidth - 60,
              marginLeft: 30,
              borderWidth: 1,
              borderColor: '#F1F2F7',
              borderRadius: 30,
              elevation: 4,
              shadowOffset: {width: 5, height: 5},
              shadowColor: '#CCCCCC',
              shadowOpacity: 0.5,
              shadowRadius: 10,
            }}>
              <TouchableOpacity disabled={isEnable ? false : this.state.isAnswerGiven ? false : this.state.updateAnswer ? false : true} onPress={()=>this.updateAnswer('terrible', this.state.isAnswerGiven,true)}>
                  {this.state.isTerribleAnswerProvided && <View style={{backgroundColor:'#87BDE2', width:60, height:25, marginTop:-40, marginBottom:15, borderRadius:12, marginLeft:20}}>
                      <Text style={{alignItems:'center',textAlign:'center', marginTop:5}}>Terrible</Text>
                  </View>}
                  <Image
                      style={{height: this.state.isTerribleAnswerProvided ? 60 : 40, width: this.state.isTerribleAnswerProvided ? 60 : 40, marginTop:this.state.isTerribleAnswerProvided ? -10 : 5, marginLeft: 20, opacity: isEnable ? 1.0 : this.state.isAnswerGiven ? 1.0 : this.state.updateAnswer ? 1.0 : 0.2}}
                      source={require('../Images/Icons/Terrible.png')}
                  />
              </TouchableOpacity>
              <TouchableOpacity disabled={isEnable ? false : this.state.isAnswerGiven ? false : this.state.updateAnswer ? false : true} onPress={()=>this.updateAnswer('bad',this.state.isAnswerGiven,true)}>
              {this.state.isBadAnswerProvided && <View style={{backgroundColor:'#87BDE2', width:60, height:25, marginTop:-40, marginBottom:15, borderRadius:12}}>
                  <Text style={{alignItems:'center',textAlign:'center', marginTop:5}}>Bad</Text>
                  </View>}
                  <Image
                  style={{height: this.state.isBadAnswerProvided ? 60 : 40, width: this.state.isBadAnswerProvided ? 60 : 40, marginTop:this.state.isBadAnswerProvided ? -10 : 5, opacity: isEnable ? 1.0 : this.state.isAnswerGiven ? 1.0 : this.state.updateAnswer ? 1.0 : 0.2}}
                  source={require('../Images/Icons/Bad.png')}
                  />
                  </TouchableOpacity>
                  <TouchableOpacity disabled={isEnable ? false : this.state.isAnswerGiven ? false : this.state.updateAnswer ? false : true} onPress={()=>this.updateAnswer('okay',this.state.isAnswerGiven,true)}>
                  {this.state.isOkayAnswerProvided && <View style={{backgroundColor:'#87BDE2', width:60, height:25, marginTop:-40, marginBottom:15, borderRadius:12}}>
                      <Text style={{alignItems:'center',textAlign:'center', marginTop:5}}>Okay</Text>
                  </View>}
                  <Image
                  style={{height: this.state.isOkayAnswerProvided ? 60 : 40, width: this.state.isOkayAnswerProvided ? 60 : 40, marginTop:this.state.isOkayAnswerProvided ? -10 : 5, opacity: isEnable ? 1.0 : this.state.isAnswerGiven ? 1.0 : this.state.updateAnswer ? 1.0 : 0.2}}
                  source={require('../Images/Icons/Okay.png')}
                  />
                  </TouchableOpacity>
                  <TouchableOpacity disabled={isEnable ? false : this.state.isAnswerGiven ? false : this.state.updateAnswer ? false : true} onPress={()=>this.updateAnswer('good',this.state.isAnswerGiven,true)}>
              {this.state.isGoodAnswerProvided && <View style={{backgroundColor:'#87BDE2', width:60, height:25, marginTop:-40, marginBottom:15, borderRadius:12}}>
                  <Text style={{alignItems:'center',textAlign:'center', marginTop:5}}>Good</Text>
                  </View>}
                  <Image
                  style={{height: this.state.isGoodAnswerProvided ? 60 : 40, width: this.state.isGoodAnswerProvided ? 60 : 40, marginTop:this.state.isGoodAnswerProvided ? -10 : 5, opacity: isEnable ? 1.0 : this.state.isAnswerGiven ? 1.0 : this.state.updateAnswer ? 1.0 : 0.2}}
                  source={require('../Images/Icons/Good.png')}
                  />
                  </TouchableOpacity>
                  <TouchableOpacity disabled={isEnable ? false : this.state.isAnswerGiven ? false : this.state.updateAnswer ? false : true} onPress={()=>this.updateAnswer('great',this.state.isAnswerGiven,true)}>
                  {this.state.isGreatAnswerProvided && <View style={{backgroundColor:'#87BDE2', width:60, height:25, marginTop:-40, marginBottom:15, borderRadius:12}}>
                      <Text style={{alignItems:'center',textAlign:'center', marginTop:5}}>Great</Text>
                  </View>}
                  <Image
                  style={{height: this.state.isGreatAnswerProvided ? 60 : 40, width: this.state.isGreatAnswerProvided ? 60 : 40, marginTop:this.state.isGreatAnswerProvided ? -10 : 5, marginRight: 20, opacity: isEnable ? 1.0 : this.state.isAnswerGiven ? 1.0 : this.state.updateAnswer ? 1.0 : 0.2}}
                  source={require('../Images/Icons/Great.png')}
                  />
                  </TouchableOpacity>
                  </View>
          </View>}
      </View>
    );
  }
}

const  style = StyleSheet.create({

    blur:{
        opacity:0.5
    }

});
