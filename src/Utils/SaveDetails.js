import {Component} from "react";
import {Alert, Platform, AsyncStorage} from 'react-native';
import {TEAMANAME, TEAMBNAME, TOURNAMENT_DETAILS} from "../Constants/StringConstants";

export async function saveTournamentDetails(tournamentDetails) {
    await AsyncStorage.setItem(TOURNAMENT_DETAILS, tournamentDetails, () => {
        console.log("Tournament name saved is====>" + tournamentDetails);
    });
}

export async function saveSurveyAnswers(question, answer) {
    await AsyncStorage.setItem(question, answer, (response) => {
        console.log("Question asked====>" + question + "and answer given ===>" + answer);
        return true;
    });
}

export async function saveFirstTeam(teamName) {
    await AsyncStorage.setItem(TEAMANAME, teamName, () => {
        console.log("First team name====>" + teamName);
    });
}

export async function saveSecondTeam(teamName) {
    await AsyncStorage.setItem(TEAMBNAME, teamName, () => {
        console.log("Second team name====>" + teamName);
    });
}

export async function getTeamBeahviour(question) {
    let questionAnswerAvailable = await AsyncStorage.getItem(question)
    return questionAnswerAvailable;
}

export async function saveToLocal(key, value) {
    await AsyncStorage.setItem(key, value, () => {
        console.log('===============> Data saved to local storage')
    });
}

export const fetchFromLocal = async function (key) {
    let value = null;
    await AsyncStorage.getItem(key, (err, result) => {
        value = result;
    });
    return value;
};


export async function removeFromLocal(key){
    try {
        await AsyncStorage.removeItem(key);
        return true;
    }
    catch(exception) {
        return false;
    }
}

export async function resetLocalStorage(){
    let userDetails= await fetchFromLocal('loggedInUserDetails');
    userDetails = await JSON.parse(userDetails);

    AsyncStorage.multiGet(['loggedInUserDetails'],(err, str)=>{
        AsyncStorage.clear(()=>{
            AsyncStorage.multiSet(str,async (err, keys)=>{
                let response = await AsyncStorage.getItem('loggedInUserDetails');
                console.log('======>' + JSON.stringify(response));
            })
        })
    })
}

