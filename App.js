/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {Dimensions, AppRegistry, AsyncStorage} from 'react-native';
import {appContainer} from './src/Screens/Navigation/Router';

export default function native() {
  class GOPRO extends React.Component {

    constructor() {
      super();
      this.state = {
        isLoggerIn: false
      }
    }

    async componentDidMount() {
      let v = await this.checkIfUserLoggedIn();
      this.setState({isLoggerIn:v});
    }

    async checkIfUserLoggedIn(){
      let loggedInUserDetails = await AsyncStorage.getItem('loggedInUserDetails');
      if (loggedInUserDetails){
        return true;
      } else {
        return false;
      }
    }



    render() {
      const AppContainer = appContainer(this.state.isLoggerIn);
      return <AppContainer />;
    }
  }

  AppRegistry.registerComponent('GOPRO', () => GOPRO);
}
